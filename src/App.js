import React from 'react'
import { Route, Switch } from "react-router-dom"

import LoginPage from './pages/login/index'

import AdminSidebar from './layouts/admin/Sidebar'
import AdminNavbar from './layouts/admin/Navbar'
import AdminFooter from './layouts/admin/Footer'

import AdminDashboard from './pages/admin/dashboard/index'
import AdminCategory from './pages/admin/category/index'
import AdminArticle from './pages/admin/article/index'
import AdminNewArticle from './pages/admin/article/new'
import AdminEditArticle from './pages/admin/article/edit'
import AdminUser from './pages/admin/user/index'
import AdminRole from './pages/admin/role/index'
import AdminAlbumGlobal from './pages/admin/album/global/index'
import AdminAlbumGlobalDetail from './pages/admin/album/global/detail'
import AdminAlbumOPD from './pages/admin/album/opd/index'
import AdminAlbumOPDDetail from './pages/admin/album/opd/detail'
import AdminAnnouncement from './pages/admin/announcement/index'
import AdminNewAnnouncement from './pages/admin/announcement/new'
import AdminEditAnnouncement from './pages/admin/announcement/edit'

import MainTop from './layouts/main/Top'
import MainNavbar from './layouts/main/Navbar'
import MainFooter from './layouts/main/Footer'

import MainHomepage from './pages/main/homepage/index'
import MainNewsGlobal from './pages/main/news/global/index'
import MainNewsGlobalDetail from './pages/main/news/global/detail'
import MainNewsOpd from './pages/main/news/opd/index'
import MainNewsOpdDetail from './pages/main/news/opd/detail'
import MainGallery from './pages/main/gallery/index'
import MainListAnnouncement from './pages/main/announcement/index'
import MainDetailAnnouncement from './pages/main/announcement/detail'

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout>
      <Component {...props} />
    </Layout>
  )} />
)

const AdminLayout = props => (
  <div className="page-container">
      <AdminSidebar />
      <div className="main-content">
        <ToastContainer />
        <AdminNavbar />
        {props.children}
        <AdminFooter />
      </div>
    </div>
)

const MainLayout = props => (
  <div>
    <MainTop />
    <MainNavbar />
    <div className="page-container container">
      <div className="main-content">
        <ToastContainer />
        {props.children}
        <MainFooter />
      </div>
    </div>
  </div>
)

const LoginLayout = props => (
  <div className="login-container">
    <ToastContainer />
    {props.children}
  </div>
)

const App = () => (
  <div>
    <Switch>
      <AppRoute exact path="/login" layout={LoginLayout} component={LoginPage} />

      <AppRoute exact path="/" layout={MainLayout} component={MainHomepage} />
      <AppRoute exact path="/berita/umum" layout={MainLayout} component={MainNewsGlobal} />
      <AppRoute exact path="/berita/umum/:id" layout={MainLayout} component={MainNewsGlobalDetail} />
      <AppRoute exact path="/berita/opd" layout={MainLayout} component={MainNewsOpd} />
      <AppRoute exact path="/berita/opd/:id" layout={MainLayout} component={MainNewsOpdDetail} />
      <AppRoute exact path="/gallery" layout={MainLayout} component={MainGallery} />
      <AppRoute exact path="/announcement" layout={MainLayout} component={MainListAnnouncement} />
      <AppRoute exact path="/announcement/:id" layout={MainLayout} component={MainDetailAnnouncement} />

      <AppRoute exact path="/admin" layout={AdminLayout} component={AdminDashboard} />
      <AppRoute exact path="/admin/article/category" layout={AdminLayout} component={AdminCategory} />
      <AppRoute exact path="/admin/article" layout={AdminLayout} component={AdminArticle} />
      <AppRoute exact path="/admin/article/new" layout={AdminLayout} component={AdminNewArticle} />
      <AppRoute exact path="/admin/article/edit/:id" layout={AdminLayout} component={AdminEditArticle} />
      <AppRoute exact path="/admin/user" layout={AdminLayout} component={AdminUser} />
      <AppRoute exact path="/admin/role" layout={AdminLayout} component={AdminRole} />
      <AppRoute exact path="/admin/album/global" layout={AdminLayout} component={AdminAlbumGlobal} />
      <AppRoute exact path="/admin/album/global/:id" layout={AdminLayout} component={AdminAlbumGlobalDetail} />
      <AppRoute exact path="/admin/album/opd" layout={AdminLayout} component={AdminAlbumOPD} />
      <AppRoute exact path="/admin/album/opd/:id" layout={AdminLayout} component={AdminAlbumOPDDetail} />
      <AppRoute exact path="/admin/announcement/" layout={AdminLayout} component={AdminAnnouncement} />
      <AppRoute exact path="/admin/announcement/new" layout={AdminLayout} component={AdminNewAnnouncement} />
      <AppRoute exact path="/admin/announcement/edit/:id" layout={AdminLayout} component={AdminEditAnnouncement} />
    </Switch>
  </div>
)


export default App;
