import { put, call } from 'redux-saga/effects'
import { SHOW_CATEGORY, FAIL_CATEGORY, FINISH_CATEGORY, CATEGORY_FETCH_STARTED } from '../redux/type/category'
import { toast } from 'react-toastify';
import { apiGetCategory, apiSaveCategory, apiRemoveCategory, apiUpdateCategory } from './api/apiCategory'

export function* getCategoryData() {
    try {
        const categoryData = yield call(apiGetCategory)
        yield put({ type : SHOW_CATEGORY, payload : categoryData })

    } catch (error) {
        yield put({ type : FAIL_CATEGORY})
        toast.error("Data kategori tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* createCategoryData(action) {
    try {
        yield put({ type : CATEGORY_FETCH_STARTED })
        let save = yield apiSaveCategory(action.payload)
        if(save)
        {
            const categoryData = yield call(apiGetCategory)
            yield put({ type : FINISH_CATEGORY})
            yield put({ type : SHOW_CATEGORY, payload : categoryData })
            toast.success("Data kategori berhasil disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
        // console.log(save)
    } catch (error) {
        yield put({ type : FINISH_CATEGORY})
        const categoryData = yield call(apiGetCategory)
        yield put({ type : SHOW_CATEGORY, payload : categoryData })
        toast.error("Data kategori tidak bisa di simpan!", {
            position: toast.POSITION.TOP_RIGHT
        });
    }
}

export function* updateCategoryData(action) {
    try {
        // yield put({ type : FINISH_CATEGORY})
        let update = yield apiUpdateCategory(action.payload)
        if(update)
        {
            const categoryData = yield call(apiGetCategory)
            yield put({ type : FINISH_CATEGORY})
            yield put({ type : SHOW_CATEGORY, payload : categoryData })
            toast.success("Data kategori berhasil diupdate!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        console.log(error)
        yield put({ type : FINISH_CATEGORY})
        const categoryData = yield call(apiGetCategory)
        yield put({ type : SHOW_CATEGORY, payload : categoryData })
        toast.error("Data kategori tidak bisa di simpan!", {
            position: toast.POSITION.TOP_RIGHT
        });
    }
}

export function* deleteCategoryData(action) {
    try {
        yield apiRemoveCategory(action.payload)
        const categoryData = yield call(apiGetCategory)
        yield put({ type : FINISH_CATEGORY})
        yield put({ type : SHOW_CATEGORY, payload : categoryData })
        toast.success("Data kategori berhasil dihapus!", {
            position: toast.POSITION.TOP_RIGHT
        });
    } catch (error) {
        yield put({ type : FINISH_CATEGORY})
        const categoryData = yield call(apiGetCategory)
        yield put({ type : SHOW_CATEGORY, payload : categoryData })
        toast.error("Data kategori tidak bisa di hapus!", {
            position: toast.POSITION.TOP_RIGHT
        });
    }
}