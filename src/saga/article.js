import { put, call } from 'redux-saga/effects'
import { SHOW_LIST_ARTICLE,ARTICLE_SUCCESS, 
    ARTICLE_FAIL, SHOW_SINGLE_ARTICLE } from '../redux/type/article'
import { toast } from 'react-toastify';
import { apiGetListArticle, apiSaveArticle, 
    apiGetSingleArticle, apiUpdateArticle} from './api/apiArticle'
import history from '../history'

export function* getListArticleData() {
    try {
        const articles = yield call(apiGetListArticle)
        yield put({ type : SHOW_LIST_ARTICLE, payload : articles })
        yield put({ type : ARTICLE_SUCCESS})
    } catch (error) {
        yield put({ type : ARTICLE_FAIL})
        toast.error("Data article tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* getSingleArticleData(action) {
    try {
        const article = yield apiGetSingleArticle(action.payload)
        yield put({ type : SHOW_SINGLE_ARTICLE, payload : article })
        yield put({ type : ARTICLE_SUCCESS})
    } catch (error) {
        yield put({ type : ARTICLE_FAIL})
        toast.error("Data article tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* saveArticleData(action) {
    try {
        const save = yield apiSaveArticle(action.payload)
        if(save)
        {
            yield put({ type: ARTICLE_SUCCESS })
            history.push('/admin/article/');
            toast.success("Data article berhasil disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put({ type : ARTICLE_FAIL})
            toast.error("Data article tidak bisa disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : ARTICLE_FAIL})
        toast.error("Data article tidak bisa disimpan!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* updateArticleData(action) {
    try {
        const save = yield apiUpdateArticle(action.payload)
        if(save)
        {
            yield put({ type: ARTICLE_SUCCESS })
            history.push('/admin/article/');
            toast.success("Data article berhasil diupdate!", {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put({ type : ARTICLE_FAIL})
            toast.error("Data article tidak bisa diupdate!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : ARTICLE_FAIL})
        toast.error("Data article tidak bisa disimpan!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}