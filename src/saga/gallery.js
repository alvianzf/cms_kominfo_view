import { put } from 'redux-saga/effects'
import { toast } from 'react-toastify';
import { apiSaveGallery } from './api/apiGallery'
import { REQUEST_GALLERY_COMPLETE, REQUEST_GALLERY_FAILED } from '../redux/type/gallery'

export function* saveGallery(action) {
    try {
        const save = yield apiSaveGallery(action.payload)
        if(save)
        {
            yield put({ type: REQUEST_GALLERY_COMPLETE })
            toast.success("Gambar berhasil disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put({ type : REQUEST_GALLERY_FAILED})
            toast.error("Gambar tidak bisa disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : REQUEST_GALLERY_FAILED})
        toast.error("Error!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}