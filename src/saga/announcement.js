import { put, call } from 'redux-saga/effects'
import { SHOW_ALL_ANNOUNCEMENT, ANNOUNCEMENT_COMPLETE,
    ANNOUNCEMENT_FAILED, SHOW_SINGLE_ANNOUNCEMENT } from '../redux/type/announcement'
import { toast } from 'react-toastify';
import history from '../history'
import { apiGetAllAnnouncement, apiSaveAnnouncement, apiGetAnnouncement, apiUpdateAnnouncement } from './api/apiAnnouncement'

export function* getAllAnnouncementData() {
    try {
        const announcements = yield call(apiGetAllAnnouncement)
        yield put({ type : SHOW_ALL_ANNOUNCEMENT, payload : announcements })
        yield put({ type : ANNOUNCEMENT_COMPLETE})
    } catch (error) {
        yield put({ type : ANNOUNCEMENT_FAILED})
        toast.error("Data pengumuman tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* getSingleAnnouncement(action) {
    try {
        const announcement = yield apiGetAnnouncement(action.payload)
        yield put({ type : SHOW_SINGLE_ANNOUNCEMENT, payload : announcement })
        yield put({ type : ANNOUNCEMENT_COMPLETE})
    } catch (error) {
        yield put({ type : ANNOUNCEMENT_FAILED})
        toast.error("Data pengumuman tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* saveAnnouncementData(action) {
    try {
        const save = yield apiSaveAnnouncement(action.payload)
        if(save)
        {
            yield put({ type: ANNOUNCEMENT_COMPLETE })
            history.push('/admin/announcement/');
            toast.success("Data pengumuman berhasil disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put({ type : ANNOUNCEMENT_FAILED})
            toast.error("Data pengumuman tidak bisa disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : ANNOUNCEMENT_FAILED})
        toast.error("Data pengumuman tidak bisa disimpan!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* updateAnnouncement(action) {
    try {
        const save = yield apiUpdateAnnouncement(action.payload)
        if(save)
        {
            yield put({ type: ANNOUNCEMENT_COMPLETE })
            history.push('/admin/announcement/');
            toast.success("Data pengumuman berhasil diupdate!", {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put({ type : ANNOUNCEMENT_FAILED})
            toast.error("Data pengumuman tidak bisa diupdate!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : ANNOUNCEMENT_FAILED})
        toast.error("Data pengumuman tidak bisa disimpan!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}