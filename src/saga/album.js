import { put, call } from 'redux-saga/effects'
import { toast } from 'react-toastify'
import { apiGetListAlbum, apiSaveAlbum, apiGetSingleAlbum, 
        apiUpdateAlbum, apiRemoveAlbum } from './api/apiAlbum'
import { REQUEST_ALBUM_COMPLETE, REQUEST_ALBUM_FAILED, 
    SHOW_LIST_ALBUM, SHOW_SINGLE_ALBUM } from '../redux/type/album'
import history from '../history'

export function* getListAlbum() {
    try {
        const albums = yield call(apiGetListAlbum)
        yield put({ type : SHOW_LIST_ALBUM, payload : albums })
        yield put({ type : REQUEST_ALBUM_COMPLETE})
    } catch (error) {
        yield put({ type : REQUEST_ALBUM_FAILED})
        toast.error("Data album tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* getSingleAlbum(action) {
    try {
        const album = yield apiGetSingleAlbum(action.payload)
        yield put({ type : SHOW_SINGLE_ALBUM, payload : album })
        yield put({ type : REQUEST_ALBUM_COMPLETE})
    } catch (error) {
        yield put({ type : REQUEST_ALBUM_FAILED})
        toast.error("Data album tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* saveAlbum(action) {
    try {
        const save = yield apiSaveAlbum(action.payload)
        if(save)
        {
            yield put({ type: REQUEST_ALBUM_COMPLETE })
            toast.success("Data album berhasil disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
            yield getListAlbum()
        } else {
            yield put({ type : REQUEST_ALBUM_FAILED})
            toast.error("Data album tidak bisa disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : REQUEST_ALBUM_FAILED})
        toast.error("Error!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* updateAlbum(action) {
    try {
        const update = yield apiUpdateAlbum(action.payload)
        if(update)
        {
            yield put({ type: REQUEST_ALBUM_COMPLETE })
            toast.success("Data album berhasil diperbaruhi!", {
                position: toast.POSITION.TOP_RIGHT
            });
            history.push('/admin/album/opd')
            yield getListAlbum()
        } else {
            yield put({ type : REQUEST_ALBUM_FAILED})
            toast.error("Data album tidak bisa diperbarui!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : REQUEST_ALBUM_FAILED})
        toast.error("Error!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* deleteAlbum(action) {
    try {
        const remove = yield apiRemoveAlbum(action.payload)
        if(remove)
        {
            yield put({ type: REQUEST_ALBUM_COMPLETE })
            toast.success("Data album berhasil dihapus!", {
                position: toast.POSITION.TOP_RIGHT
            });
            history.push('/admin/album/opd')
            yield getListAlbum()
        } else {
            yield put({ type : REQUEST_ALBUM_FAILED})
            toast.error("Data album tidak bisa dihapus!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : REQUEST_ALBUM_FAILED})
        toast.error("Error!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}