import { put } from 'redux-saga/effects'
import { LOGIN_SUCCESS } from '../redux/type/login'
import { setToken, unsetToken } from '../redux/action/token'
import { loginFail } from '../redux/action/login'
import { postNoAuth } from '../lib/fetch'
import history from '../history'
import { toast } from 'react-toastify';

export function* login (action) {
    try {
        let loginData = yield postNoAuth(JSON.stringify(action.payload), 'users/login')
        .then(body => body.json())
        .then(res => res)
        .catch(err => console.log(err))

        if(loginData && loginData.success) {
            yield put({ type : LOGIN_SUCCESS })
            yield put(setToken(loginData.data.token))
            yield localStorage.setItem('token', loginData.data.token )
            yield localStorage.setItem('role', loginData.data.role._id )
            history.push('/admin');
        } else {
            toast.error("NIP Atau Password Salah !", {
                position: toast.POSITION.TOP_RIGHT
            });
            yield put(loginFail('NIP Atau Password Salah'))
            
            yield put(unsetToken())
            localStorage.removeItem('token')
        } 
        
    } catch (error) {
        console.log(error)
        yield put(loginFail('Mohon Hubungi Administrator'))
    }
}


