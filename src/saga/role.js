import { put, call } from 'redux-saga/effects'
import { GET_ROLE_SUCCESS, SEND_ROLE_SUCCESS, ROLE_FAIL } from '../redux/type/role'
import { toast } from 'react-toastify';
import { apiGetRole, apiPostRole, apiRemoveRole, apiUpdateRole } from './api/apiRole'

export function* getListRoleData() {
    try {
        const roles = yield call(apiGetRole)
        roles ? yield put({ type : GET_ROLE_SUCCESS, payload : roles })
         : yield put({ type : ROLE_FAIL})
    } catch (error) {
        yield put({ type : ROLE_FAIL})
        toast.error("Data role tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* saveRoleData(action) {
    try {
        const save = yield apiPostRole(action.payload)
        console.log(save)
        if(save) 
        {
            yield put({ type: SEND_ROLE_SUCCESS }) 
            const roles = yield call(apiGetRole)
            yield put({ type : GET_ROLE_SUCCESS, payload : roles })
            toast.success("Data role berhasil disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put({ type : ROLE_FAIL})
            toast.error("Data role tidak bisa disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }

    } catch (error) {
        yield put({ type : ROLE_FAIL})
        toast.error("Error! Hubungi Administrator", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* updateRoleData(action) {
    try {
        const update = yield apiUpdateRole(action.payload)
        if(update) 
        {
            yield put({ type: SEND_ROLE_SUCCESS }) 
            const roles = yield call(apiGetRole)
            yield put({ type : GET_ROLE_SUCCESS, payload : roles })
            toast.success("Data role berhasil diupdate!", {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put({ type : ROLE_FAIL})
            toast.error("Data role tidak bisa disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }

    } catch (error) {
        yield put({ type : ROLE_FAIL})
        toast.error("Error! Hubungi Administrator", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* deleteRoleData(action){
    try {
        yield apiRemoveRole(action.payload)
        yield put({ type: SEND_ROLE_SUCCESS }) 
        const roles = yield call(apiGetRole)
        yield put({ type : GET_ROLE_SUCCESS, payload : roles })
        toast.success("Data role berhasil dihapus!", {
            position: toast.POSITION.TOP_RIGHT
        });
    } catch (error) {
        yield put({ type : ROLE_FAIL})
        toast.error("Error! Hubungi Administrator", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}