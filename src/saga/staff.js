import { put, call } from 'redux-saga/effects'
import { SHOW_STAFF_DATA, COMPLETE_STAFF_DATA, FAIL_STAFF_DATA } from '../redux/type/staff'
import { toast } from 'react-toastify';
import { apiGetStaffData } from './api/apiStaff'

export function* getStaffData() {
    try {
        const users = yield call(apiGetStaffData)
        if(users) {
            yield put({ type : SHOW_STAFF_DATA, payload : users })
            yield put({ type : COMPLETE_STAFF_DATA })
        } else {
            yield put({ type : FAIL_STAFF_DATA})
            toast.error("Data Pegawai Tidak Bisa Diambil!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : FAIL_STAFF_DATA})
        toast.error("Error! Hubungi Administrator", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}