import { put } from 'redux-saga/effects'
import { toast } from 'react-toastify';
import { SHOW_LIST_COMMENT, COMPLETE_FETCH_COMMENT, 
    FAIL_FETCH_COMMENT } from '../redux/type/comment'
import { apiGetListComment, apiSaveComment } from './api/apiComment'

export function* getListComment(action) {
    try {
        const comments = yield apiGetListComment(action.payload)
        yield put({ type : SHOW_LIST_COMMENT, payload : comments })
        yield put({ type : COMPLETE_FETCH_COMMENT})
    } catch (error) {
        yield put({ type : FAIL_FETCH_COMMENT})
        toast.error("Data comment tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* saveCommentData(action) {
    try {
        const save = yield apiSaveComment(action.payload)
        if(save)
        {
            const slug = {
                payload : action.payload.slug
            }
            yield put({ type: COMPLETE_FETCH_COMMENT })
            yield getListComment(slug)
            toast.success("comment berhasil disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put({ type : FAIL_FETCH_COMMENT})
            toast.error("Data article tidak bisa diambil!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : FAIL_FETCH_COMMENT})
        toast.error("Error! Hubungi Administrator", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}