import { put, call } from 'redux-saga/effects'
import { SHOW_LIST_OPD, OPD_FAIL } from '../redux/type/opd'
import { toast } from 'react-toastify';
import { apiGetListOpd } from './api/apiOpd'

export function* getListOpdData() {
    try {
        const opds = yield call(apiGetListOpd)
        opds ? yield put({ type : SHOW_LIST_OPD, payload : opds })
         : yield put({ type : OPD_FAIL})
    } catch (error) {
        yield put({ type : OPD_FAIL})
        toast.error("Data opd tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}