import { postImg } from '../../lib/fetch'

export const apiSaveGallery = (data) => {
    const formData = new FormData()    
    formData.set("album", data.album)
    formData.append(
        "photos",
        data.thumbnail
    )
    return postImg(formData, 'gallery')
    .then(res => res.json())
    .then(json => {
        return json.success
    })
}