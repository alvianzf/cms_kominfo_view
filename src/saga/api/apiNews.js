import { get } from '../../lib/fetch'

export const apiGetListNews = () => {
    return get('berita-pemko/all')
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiGetAllNews = () => {
    return get('berita-pemko/all')
    .then(res => res.json())
    .then(json => json.data)
}

export const apiGetSingleNew = (data) => {
    return get('berita-pemko/'+data)
    .then(res => res.json())
    .then(json => json.data)
}