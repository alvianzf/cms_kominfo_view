import { get, post} from '../../lib/fetch'

const API_URL = process.env.REACT_APP_API_URL
const token = localStorage.getItem('token')

export const apiGetCategory = () => {
    return get('category')
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiSaveCategory = (data) => {
    return post(data,'category')
    .then(res => res.json())
    .then(json => {
        return json.success
    })
}

export const apiRemoveCategory = (data) => {
    
    return fetch(API_URL + 'category/' + data, {
        method : 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : `Token ${token}`
        }
    })
    .then(res => {
        return 'testing'
    })
    
}

export const apiUpdateCategory = (data) => {
    return fetch(API_URL + 'category/' + data.id, {
        method : 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : `Token ${token}`,
        },
        body : JSON.stringify(data)
    })
    .then(res => res.json())
    .then(json => {
        return true
    })
}