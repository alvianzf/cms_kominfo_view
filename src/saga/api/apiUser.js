import { get, postImg } from '../../lib/fetch'

export const apiGetListUser = () => {
    return get('users')
    .then(res => res.json())
    .then(json => json.data)
}

export const apiGetDetailUser = () => {
    return get('user')
    .then(res => res.json())
    .then(json => json.data)
}

export const apiSaveUser = (data) => {
    const formData = new FormData()    
    formData.set("email", data.email)
    formData.set("idPeg", data.idPeg)
    formData.set("name", data.name)
    formData.set("password", data.password)
    formData.append(
        "avatar",
        data.avatar
    )
    return postImg(formData, 'users')
    .then(res => res.json())
    .then(json => {
        return json.success
    })
}

export const apiActiveUser = () => {
    return get('user')
    .then(res => res.json())
    .then(json => json.data)
}