import { getNoAuth, post } from '../../lib/fetch'

export const apiGetListComment = (data) => {
    return getNoAuth(`articles/${data}/comments`)
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiSaveComment = (data) => {
    const saveData = JSON.stringify(data)
    const slug = data.slug
    const api = `articles/${slug}/comments`
    return post(saveData,api)
    .then(res => res.json())
    .then(json => {
        return json.success
    })
}