import { get,post, put,  } from '../../lib/fetch'

const API_URL = process.env.REACT_APP_API_URL
const token = localStorage.getItem('token')

export const apiGetListAlbum = () => {
    return get('album')
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiGetSingleAlbum = (data) => {
    return get('album/'+ data)
    .then(res  => res.json())
    .then(json => {
        return json.data
    })
}

export const apiSaveAlbum = (data) => {
    return post(data, 'album')
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiUpdateAlbum = (data) => {
    return put(JSON.stringify(data.data), 'album/'+data.slug.slug)
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiRemoveAlbum = (data) => {
    return fetch(API_URL + 'album/' + data, {
        method : 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : `Token ${token}`
        }
    })
    .then(res => {
        return 'testing'
    })
}