import { get, putImg, postImg } from '../../lib/fetch'

export const apiGetAllAnnouncement = () => {
    return get('pengumuman-dinas/')
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiGetAnnouncement = (data) => {
    return get('pengumuman-dinas/'+data)
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiSaveAnnouncement = data => {
    const formData = new FormData()    
    formData.set("title", data.title)
    formData.set("description", data.description)
    formData.set("body", data.body)
    formData.set("category", data.category)
    formData.set("opd", data.opd)
    formData.set("status", data.status)
    formData.append(
        "thumbnail",
        data.thumbnail
    )
    return postImg(formData, 'pengumuman-dinas')
    .then(res => res.json())
    .then(json => {
        return json.success
    })
}

export const apiUpdateAnnouncement = data => {
    const formData = new FormData()    
    formData.set("title", data.title)
    formData.set("description", data.description)
    formData.set("body", data.body)
    formData.set("category", data.category)
    formData.set("opd", data.opd)
    formData.set("status", data.status)
    formData.append(
        "thumbnail",
        data.thumbnail
    )
    return putImg(formData, 'pengumuman-dinas/'+data.slug)
    .then(res => res.json())
    .then(json => {
        return json.success
    })
}