import { get } from '../../lib/fetch'

export const apiGetListOpd = () => {
    return get('opd')
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}