import { get, putImg, postImg } from '../../lib/fetch'

export const apiGetListArticle = () => {
    return get('articles')
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiGetSingleArticle = (data) => {
    return get('articles/'+data)
    .then(res => res.json())
    .then(json => {
        return json.data
    })
}

export const apiGetListGlobalArticle = () => {
    return get('berita-pemko')
    .then(res => res.json())
    .then(json => json.data)
}

export const apiSaveArticle = (data) => {
    const formData = new FormData()    
    formData.set("title", data.title)
    formData.set("description", data.description)
    formData.set("body", data.body)
    formData.set("category", data.category)
    formData.set("opd", data.opd)
    formData.set("status", data.status)
    formData.append(
        "thumbnail",
        data.thumbnail
    )
    return postImg(formData, 'articles')
    .then(res => res.json())
    .then(json => {
        return json.success
    })
}

export const apiUpdateArticle = (data) => {
    const formData = new FormData()    
    formData.set("title", data.title)
    formData.set("description", data.description)
    formData.set("body", data.body)
    formData.set("category", data.category)
    formData.set("opd", data.opd)
    formData.set("status", data.status)
    return putImg(formData, 'articles/'+data.slug)
    .then(res => res.json())
    .then(json => {
        return json.success
    })
}