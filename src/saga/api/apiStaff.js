import { get } from '../../lib/fetch'

export const apiGetStaffData = () => {
    return get('pegawai')
    .then(res => res.json())
    .then(json => json.data)
}