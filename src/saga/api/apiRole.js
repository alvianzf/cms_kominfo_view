import { get, post } from '../../lib/fetch'

const API_URL = process.env.REACT_APP_API_URL
const token = localStorage.getItem('token')

export const apiGetRole = () => {
    return get('role')
    .then(res => res.json())
    .then(json => json.data)
}

export const apiUpdateRole = (data) => {
    return fetch(API_URL + 'role/' + data.id, {
        method : 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : `Token ${token}`,
        },
        body : JSON.stringify(data)
    })
    .then(res => res.json())
    .then(json => json)
}

export const apiRemoveRole = (data) => {
    
    return fetch(API_URL + 'role/' + data, {
        method : 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : `Token ${token}`
        }
    })
    .then(res => { return true})
    
}

export const apiPostRole = (data) => {
    return post(data, 'role')
    .then(res => res.json())
    .then(json => json.success)
}