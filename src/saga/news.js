import { put, call } from 'redux-saga/effects'
import { SHOW_NEWS_LIST_DATA,NEWS_SUCCESS, 
    NEWS_FAIL, SHOW_NEWS_ALL_DATA, SHOW_NEWS_SINGLE_DATA } from '../redux/type/news'
import { toast } from 'react-toastify';
import { apiGetListNews, apiGetAllNews, apiGetSingleNew } from './api/apiNews'

export function* getListNewsData() {
    try {
        const articles = yield call(apiGetListNews)
        yield put({ type : SHOW_NEWS_LIST_DATA, payload : articles })
        yield put({ type : NEWS_SUCCESS})
    } catch (error) {
        yield put({ type : NEWS_FAIL})
        toast.error("Data berita tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* getAllNewsData() {
    try {
        const articles = yield call(apiGetAllNews)
        yield put({ type : SHOW_NEWS_ALL_DATA, payload : articles })
        yield put({ type : NEWS_SUCCESS})
    } catch (error) {
        yield put({ type : NEWS_FAIL})
        toast.error("Data berita tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* getSingleNewsData(action) {
    try {
        const article = yield apiGetSingleNew(action.payload)
        yield put({ type : SHOW_NEWS_SINGLE_DATA, payload : article })
        yield put({ type : NEWS_SUCCESS})
    } catch (error) {
        yield put({ type : NEWS_FAIL})
        toast.error("Data berita tidak bisa diambil!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}