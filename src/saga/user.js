import { put, call } from 'redux-saga/effects'
import {  SHOW_DETAIL_USER, SHOW_LIST_USER, USER_FAIL_FETCH, 
        USER_FETCH_COMPLETE, COMPLETE_USER, SHOW_ACTIVE_USER } from '../redux/type/user'
import { toast } from 'react-toastify';
import { apiGetDetailUser, apiGetListUser, apiSaveUser, apiActiveUser } from './api/apiUser'

export function* getListUser() {
    try {
        const users = yield call(apiGetListUser)
        if(users) {
            yield put({ type : SHOW_LIST_USER, payload : users })
            yield getDetailUser()
            yield put({ type : USER_FETCH_COMPLETE })
        } else {
            yield put({ type : USER_FAIL_FETCH})
            toast.error("Data List User Tidak Bisa Diambil!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : USER_FAIL_FETCH})
        toast.error("Error! Hubungi Administrator", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* getDetailUser() {
    try {
        const user = yield call(apiGetDetailUser)
        if(user) {
            yield put({ type : SHOW_DETAIL_USER, payload : user })
            yield put({ type : USER_FETCH_COMPLETE })
        } else {
            yield put({ type : USER_FAIL_FETCH})
            toast.error("Data List User Tidak Bisa Diambil!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : USER_FAIL_FETCH})
        toast.error("Error! Hubungi Administrator", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* saveUserData(action) {
    try {
        const save = yield apiSaveUser(action.payload)
        if(save)
        {
            yield put({ type: COMPLETE_USER })
            yield getListUser()
            toast.success("Data user berhasil disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put({ type : USER_FAIL_FETCH})
            toast.error("Data suer tidak bisa disimpan!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : USER_FAIL_FETCH})
        toast.error("Data article tidak bisa disimpan!", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}

export function* getActiveUser() {
    try {
        const user = yield call(apiActiveUser)
        if(user) {
            yield put({ type : SHOW_ACTIVE_USER, payload : user })
            yield getDetailUser()
            yield put({ type : USER_FETCH_COMPLETE })
        } else {
            yield put({ type : USER_FAIL_FETCH})
            toast.error("Data User Tidak Bisa Diambil!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put({ type : USER_FAIL_FETCH})
        toast.error("Error! Hubungi Administrator", {
            position: toast.POSITION.TOP_RIGHT
        });
        console.log(error)
    }
}