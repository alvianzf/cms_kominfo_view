
import { all, takeLatest } from 'redux-saga/effects'
import { LOGIN_REQUEST } from '../redux/type/login'
import { FETCH_CATEGORY, CREATE_CATEGORY, DELETE_CATEGORY, UPDATE_CATEGORY } from '../redux/type/category'
import { login } from './login'
import { getCategoryData, createCategoryData, deleteCategoryData, updateCategoryData} from './category'
import { GET_LIST_ARTICLE,SAVE_ARTICLE, GET_SINGLE_ARTICLE,
    UPDATE_ARTICLE } from '../redux/type/article'
import { getListArticleData, saveArticleData, 
    getSingleArticleData, updateArticleData } from './article'
import { GET_ROLE,SEND_ROLE , DELETE_ROLE, UPDATE_ROLE} from '../redux/type/role'
import { getListRoleData, saveRoleData, deleteRoleData, updateRoleData } from './role'
import { GET_LIST_USER, CREATE_USER, GET_ACTIVE_USER } from '../redux/type/user'
import { getListUser, saveUserData, getActiveUser } from './user'
import { GET_STAFF_DATA } from '../redux/type/staff'
import { getStaffData } from './staff'
import { GET_LIST_OPD } from '../redux/type/opd'
import { getListOpdData } from './opd'
import { getListNewsData, getAllNewsData,getSingleNewsData } from './news'
import { GET_NEWS_LIST_DATA, GET_NEWS_ALL_DATA, GET_NEWS_SINGLE_DATA } from '../redux/type/news'
import { GET_LIST_COMMENT, CREATE_COMMENT } from '../redux/type/comment'
import { getListComment, saveCommentData } from './comment'
import { GET_LIST_ALBUM, CREATE_ALBUM, GET_SINGLE_ALBUM, UPDATE_ALBUM, DELETE_ALBUM } from '../redux/type/album'
import { getListAlbum, saveAlbum, getSingleAlbum, updateAlbum, deleteAlbum } from './album'
import { CREATE_GALLERY } from '../redux/type/gallery'
import { saveGallery } from './gallery'
import { GET_ALL_ANNOUNCEMENT, SAVE_ANNOUNCEMENT, GET_SINGLE_ANNOUNCEMENT, EDIT_ANNOUNCEMENT } from '../redux/type/announcement'
import { getAllAnnouncementData, saveAnnouncementData, getSingleAnnouncement, updateAnnouncement } from './announcement'

export default function* IndexSaga() {
    yield all([
        takeLatest( LOGIN_REQUEST, login ),
        takeLatest( FETCH_CATEGORY , getCategoryData),
        takeLatest( CREATE_CATEGORY , createCategoryData),
        takeLatest( DELETE_CATEGORY, deleteCategoryData),
        takeLatest( UPDATE_CATEGORY, updateCategoryData),
        takeLatest( GET_LIST_ARTICLE, getListArticleData),
        takeLatest( GET_SINGLE_ARTICLE, getSingleArticleData),
        takeLatest( SAVE_ARTICLE ,saveArticleData),
        takeLatest( UPDATE_ARTICLE, updateArticleData),
        takeLatest( GET_ROLE, getListRoleData),
        takeLatest( SEND_ROLE, saveRoleData),
        takeLatest( DELETE_ROLE, deleteRoleData),
        takeLatest( UPDATE_ROLE, updateRoleData),
        takeLatest( GET_LIST_USER, getListUser),
        takeLatest( CREATE_USER, saveUserData),
        takeLatest( GET_ACTIVE_USER, getActiveUser),
        takeLatest( GET_STAFF_DATA, getStaffData),
        takeLatest( GET_LIST_OPD, getListOpdData),
        takeLatest( GET_NEWS_LIST_DATA, getListNewsData ),
        takeLatest( GET_NEWS_ALL_DATA , getAllNewsData ),
        takeLatest( GET_NEWS_SINGLE_DATA, getSingleNewsData),
        takeLatest( GET_LIST_COMMENT, getListComment),
        takeLatest( CREATE_COMMENT, saveCommentData),
        takeLatest( GET_LIST_ALBUM, getListAlbum),
        takeLatest( CREATE_ALBUM, saveAlbum),
        takeLatest( GET_SINGLE_ALBUM, getSingleAlbum),
        takeLatest( UPDATE_ALBUM, updateAlbum),
        takeLatest( DELETE_ALBUM, deleteAlbum),
        takeLatest( CREATE_GALLERY, saveGallery),
        takeLatest( GET_ALL_ANNOUNCEMENT, getAllAnnouncementData),
        takeLatest( SAVE_ANNOUNCEMENT, saveAnnouncementData ),
        takeLatest( GET_SINGLE_ANNOUNCEMENT, getSingleAnnouncement),
        takeLatest( EDIT_ANNOUNCEMENT, updateAnnouncement)
    ])
}