import React, { Component } from 'react'

class ListEvent extends Component {
    render() {
        return (
            <div className="panel panel-default">
                <div className="panel-heading judul-panel">
                    KALENDER EVENT
                    <div className="panel-options">
                        <a href="#0" data-toggle="panel">
                            <span className="collapse-icon">–</span>
                            <span className="expand-icon">+</span>
                        </a>
                    </div>
                </div>
                <div className="panel-body">
                    <ul className="kalender-event">
                        <li>
                            <a href="#0">
                                <h3>
                                    <div className="bulan"> Nov </div>
                                    29
                                    <div className="tahun"> 2019 </div>
                                </h3>
                                <h5>Pekan Olah Raga Provinsi Kepulauan Riau 2019</h5>
                                <small>Tanjung Pinang</small>
                            </a>
                        </li>
                        <li>
                            <a href="#0">
                                <h3>
                                    <div className="bulan"> Nov </div>
                                    29
                                    <div className="tahun"> 2019 </div>
                                </h3>
                                <h5>Pekan Olah Raga Provinsi Kepulauan Riau 2019</h5>
                                <small>Tanjung Pinang</small>
                            </a>
                        </li>
                        <li>
                            <a href="#0">
                                <h3>
                                    <div className="bulan"> Nov </div>
                                    29
                                    <div className="tahun"> 2019 </div>
                                </h3>
                                <h5>Pekan Olah Raga Provinsi Kepulauan Riau 2019</h5>
                                <small>Tanjung Pinang</small>
                            </a>
                        </li>
                        <li>
                            <a href="#0">
                                <h3>
                                    <div className="bulan"> Nov </div>
                                    29
                                    <div className="tahun"> 2019 </div>
                                </h3>
                                <h5>Pekan Olah Raga Provinsi Kepulauan Riau 2019</h5>
                                <small>Tanjung Pinang</small>
                            </a>
                        </li>
                    </ul>
                    <button className="mg-t-15 pull-right btn btn-warning btn-icon btn-icon-standalone btn-icon-standalone-right btn-xs">
                        <i className="fa-arrow-right"></i><span>Selengkapnya</span>
                    </button>
                </div>
            </div>
            
        )
    }
}

export default ListEvent