import React, { Component } from 'react'

class Loading extends Component {
    render() {
        return (
            <div className="page-loading-overlay">
                <div className="loader-2" />
            </div>
        )
    }
}

export default Loading