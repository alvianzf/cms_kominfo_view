import React , { Component } from 'react'

class ColumnNews extends Component {
    render() {
        return (
            <div className="xe-widget xe-status-update" data-auto-switch="3">
                <div className="xe-header">
                    <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                </div>
                <div className="xe-body">   
                    <ul className="list-unstyled mg-t-15">
                        <li className="active">
                            <p>Build your own Fake Twitter Post now! Check it out @ simitator.com #laborator #envato</p>
                        </li>
                    </ul>
                </div>
                <div className="xe-footer">
                    <button className="btn btn-warning">View</button>
                </div>
            </div>
        )
    }
}

export default ColumnNews