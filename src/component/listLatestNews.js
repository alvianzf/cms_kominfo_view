import React , { Component } from 'react'
import { Link } from 'react-router-dom';

class ListLatestNews extends Component {
    render() {
        const renderList = this.props.news.map( news => (
            <li key={news.id_berita}>
                <Link to={"/berita/umum/"+news.link_berita}>
                    <div className="gambar-post-sidebar">
                        <img className="img-responsive center-block" src={news.link_gambar_small} alt="" />
                    </div>
                    <h5> {news.judul_berita} </h5>
                    <p style={{ textAlign : 'justify' }}> </p>
                </Link>
            </li>
        ))
        return (
            <div className="panel panel-default">
                <div className="panel-heading judul-panel">
                    BERITA TERKINI
                    <div className="panel-options">
                        <a href="#0" data-toggle="panel">
                            <span className="collapse-icon">–</span>
                            <span className="expand-icon">+</span>
                        </a>
                    </div>
                </div>
                <div className="panel-body">
                    <ul className="thumb-list">
                        {renderList}
                    </ul>
                </div>
            </div>
        )
    }
}

export default ListLatestNews