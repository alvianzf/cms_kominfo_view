import React, { Component } from 'react'

class Navbar extends Component {
    render() {
        return (
            <nav className="navbar user-info-navbar" role="navigation">
				<ul className="user-info-menu left-links list-inline list-unstyled">
					<li className="hidden-sm hidden-xs">
						<a href="#0" data-toggle="sidebar">
							<i className="fa-bars"></i>
						</a>
					</li>
				</ul>
				<ul className="user-info-menu right-links list-inline list-unstyled">
					<li className="dropdown user-profile">
						<a href="#0" data-toggle="dropdown">
							<span>
								Arlind Nushi &nbsp;
								<i className="fa-angle-down"></i>
							</span>
						</a>
						<ul className="dropdown-menu user-profile-menu list-unstyled">
							<li className="last">
								<a href="#0">
									<i className="fa-lock"></i>
									Logout
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</nav>
        )
    }
}

export default Navbar