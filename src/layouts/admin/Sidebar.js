import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { getActiveUser } from '../../redux/action/user'

class Sidebar extends Component {

    componentDidMount()
    {
        this.props.getActiveUserData()
    }

    render() {
      return (
        <div className="sidebar-menu toggle-others fixed">
            <div className="sidebar-menu-inner">	
                <header className="logo-env">
                    <div className="logo">
                        <Link to="/" className="logo-expanded">
                            CMS
                        </Link>
                        <Link to="/" className="logo-collapsed">
                            CMS
                        </Link>
                    </div>
                    <div className="mobile-menu-toggle visible-xs">
                        <a href="#0" data-toggle="mobile-menu">
                            <i className="fa-bars"></i>
                        </a>
                    </div>		
                </header>    
                <section className="sidebar-user-info">
                    <div className="sidebar-user-info-inner">
                        <div className="user-adm">
                            <a className="user-profile" href="#0">
                                <img className="img-circle img-corona" src="#0" alt="user-pic"/>
                                <span>
                                    <strong>{this.props.user.actived ? this.props.user.actived.name : null }</strong>  
                                    
                                </span>
                            </a>
                            <ul className="user-links list-unstyled">
                                <li>
                                    <a href="#0" title="Edit profile">
                                        <i className="linecons-user"></i>  Profil
                                    </a>
                                </li>
                                <li>
                                    <a href="#9" title="Pengaturan Sistem">
                                        <i className="fa-cog"></i>  Pengaturan
                                    </a>
                                </li>
                                <li className="logout-link">
                                    <a href="#0" title="Log out">
                                        <i className="fa-power-off"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>           
                <ul id="main-menu" className="main-menu">
                    <li>
                        <Link to="/admin">
                            <i className="fa-home"></i>
							<span className="title">Beranda</span>
                        </Link>
					</li>
                    <li className="has-sub">
						<a href="#0">
							<i className="fa-file-text"></i>
							<span className="title">Artikel</span>
						</a>
						<ul>
							<li>
								<Link to="/admin/article/category/">
									<span className="title">Kategori</span>
								</Link>
							</li>
                            <li>
								<Link to="/admin/article/">
									<span className="title">Data Artikel</span>
								</Link>
							</li>
						</ul>
					</li>
                    <li className="has-sub">
						<a href="#0">
							<i className="fa-image"></i>
							<span className="title">Album</span>
						</a>
						<ul>
                            <li>
                                <Link to="/admin/album/global">
                                    <span className="title">Album Global</span>
                                </Link>
                            </li>
                            <li>
                                <Link to="/admin/album/opd">
                                    <span className="title">Album OPD</span>
                                </Link>
                            </li>
						</ul>
					</li>
                    <li>
                        <Link to="/admin/announcement/">
                            <i className="fa-bullhorn"></i>
							<span className="title">Pengumuman</span>
                        </Link>
					</li>
                    <li className="has-sub">
                        <a href="#1">
                            <i className="fa-user"></i>
							<span className="title">User</span>
                        </a>
                        <ul>
							<li>
                                <Link to="/admin/user">
                                    <span className="title">Users</span>
                                </Link>
							</li>
                            <li>
								<Link to="/admin/role">
									<span className="title">Role</span>
								</Link>
							</li>
						</ul>
					</li>
                </ul>
            </div>
        </div>
      );
    }
  }

const mapStateToProps = state => ({
    user : state.user
})

const mapDispatchToProps = dispatch => {
    return {
        getActiveUserData : () => dispatch(getActiveUser())
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);