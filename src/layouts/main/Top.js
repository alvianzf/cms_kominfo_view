import React, { Component } from 'react'

class Top extends Component {
    render() {
        return (
            <div className="topbanner">
                <div className="container">
                    <div className="col-md-12">
                        <div className="col-md-9 no-padding">
                            <a href=".">
                                <div className="logopemko">&nbsp;</div>
                                <div className="ketlogo">
                                    <div className="ketlogopemko">  
                                        PEMERINTAH KOTA TANJUNGPINANG
                                    </div>
                                    <div className="ketlogoinstansi">
                                        Dinas Komunikasi dan Informatika
                                    </div>
                                    <div className="ketlogoalamat1">  
                                        Jl. Gatot Subroto Km. 5 Bawah, Kelurahan Kampung Bulang, Kecamatan Tanjungpinang Timur, Kota Tanjungpinang, Provinsi Kepulauan Riau
                                    </div>
                                    <div className="ketlogoalamat2">  
                                        Telp. (+62 771) 808 1394, Email: kominfo@tanjungpinangkota.go.id, Website: kominfo.tanjungpinangkota.go.id
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Top