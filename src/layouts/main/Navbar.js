import React, { Component } from 'react'
import { Link } from 'react-router-dom';

class Navbar extends Component {
    render() {
        return (
            <nav className="navbar horizontal-menu nav-minimal">
                <div className="page-container container">
                    <div className="navbar-inner">
                        <div className="navbar-brand visible-xs">
                            <a className="logo" href=".">
                                <img className="hidden-xs" src={require('../../assets/img/logodiskominfo.png')} width="180" alt=""/>
                                <img className="visible-xs" src={require('../../assets/img/logodiskominfo.png')} width="180" alt=""/>
                            </a>
                        </div>
                        <ul className="navbar-nav">
                            <li>
                                <Link to="/">
                                    <i className="fa-home"></i>
                                    <span className="title">Beranda</span>
                                </Link>
                            </li>
                            <li className="has-sub">
                                <a href="#0">
                                    <i className="linecons-note"></i>
                                    <span className="title"> Berita </span>
                                </a>
                                <ul>
                                    <li>
                                        <Link to="/berita/umum">
                                            <span className="sub-title">Berita Pemko</span>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/berita/opd">
                                            <span className="sub-title">Berita Dinas</span>
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <Link to="/gallery">
                                    <i className="linecons-photo"></i>
                                    <span className="title">Gallery</span>
                                </Link>
                            </li>
                            <li>
                                <Link to="/announcement">
                                    <i className="linecons-megaphone"></i>
                                    <span className="title">Pengumuman</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav> 
        )
    }
}

export default Navbar