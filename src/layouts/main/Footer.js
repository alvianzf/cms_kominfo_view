import React, { Component } from 'react'

class Footer extends Component {
    render() {
        const date = new Date()
        return (
            <footer className="main-footer sticky footer-type-1">
				<div className="footer-inner">
					<div className="footer-text">
						&copy; {date.getFullYear()} -  
						<strong> Diskominfo Tanjung Pinang </strong> 
					</div>
					<div className="go-up">
						<a href="#0" rel="go-top">
							<i className="fa-angle-up"></i>
						</a>
					</div>
				</div>
			</footer>
        )
    }
}

export default Footer;