import {GET_NEWS_LIST_DATA, GET_NEWS_SINGLE_DATA,
    SHOW_NEWS_LIST_DATA, SHOW_NEWS_SINGLE_DATA,
    NEWS_SUCCESS,  NEWS_FAIL, SHOW_NEWS_ALL_DATA,
    GET_NEWS_ALL_DATA } from '../type/news'

const initialState = {
    loading : false,
    data : [],
    detail : null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_NEWS_LIST_DATA :
            return {
                ...state,
                loading: true
            }
        case GET_NEWS_SINGLE_DATA :
            return {
                ...state,
                loading: true
            }
        case SHOW_NEWS_LIST_DATA :
            return {
                ...state,
                loading: false,
                data : action.payload
            }
        case SHOW_NEWS_SINGLE_DATA : 
            return {
                ...state,
                detail : action.payload,
                loading : false
            }
        case NEWS_SUCCESS :
            return {
                ...state,
                loading : false
            }
        case NEWS_FAIL :
            return {
                ...state,
                loading : false
            }
        case GET_NEWS_ALL_DATA :
            return {
                ...state,
                loading : true
            }
        case SHOW_NEWS_ALL_DATA :
            return {
                ...state,
                loading : false,
                data : action.payload
            }
        default :
            return state
    }
}