import { FETCH_CATEGORY,SHOW_CATEGORY, FAIL_CATEGORY, CREATE_CATEGORY, 
    FINISH_CATEGORY,CATEGORY_FETCH_STARTED,DELETE_CATEGORY,
    UPDATE_CATEGORY } from '../type/category'

const initialState = {
    loading : false,
    data : [],
    message : null,
    payload : null,
    name : null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case CATEGORY_FETCH_STARTED :
            return {
                loading: true
            }
        case FETCH_CATEGORY :
            return {
                ...initialState,
                loading : true,
                message : null
            }
        case SHOW_CATEGORY :
            return {
                loading : false,
                data : action.payload
            }
        case CREATE_CATEGORY:
            return {
                loading : true,
                payload : action.payload
            }
        case UPDATE_CATEGORY:
            return {
                loading : true,
                payload : action.payload
            }
        case DELETE_CATEGORY:
            return {
                loading : true,
                payload : action.payload
            }
        case FINISH_CATEGORY:
            return {
                ...initialState,
                loading : false,
                payload : null,
                name : null
            }
        case FAIL_CATEGORY :
            return {
                loading : false,
                data : null,
                message : 'Failed to get category'
            }
        default :
            return state
    }
}