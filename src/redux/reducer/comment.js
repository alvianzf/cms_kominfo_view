import { GET_LIST_COMMENT , SHOW_LIST_COMMENT ,
    CREATE_COMMENT, DELETE_COMMENT, FAIL_FETCH_COMMENT,
    COMPLETE_FETCH_COMMENT } from '../type/comment'

const initialState = {
    loading : false,
    data : []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_LIST_COMMENT :
            return {
                ...state,
                loading : true
            }
        case SHOW_LIST_COMMENT :
            return {
                ...state,
                loading : false,
                data : action.payload
            }
        case CREATE_COMMENT :
            return {
                ...state,
                loading : true
            }
        case DELETE_COMMENT :
            return {
                ...state,
                loading : true
            }
        case FAIL_FETCH_COMMENT :
            return {
                ...state,
                loading : false
            }
        case COMPLETE_FETCH_COMMENT :
            return {
                ...state,
                loading : false
            }
        default :
            return state
    }
}