import { GET_ALL_GALLERY_DATA, SHOW_ALL_GALLERY_DATA,
    CREATE_GALLERY, UPDATE_GALLERY, DELETE_GALLERY,
    REQUEST_GALLERY_COMPLETE, REQUEST_GALLERY_FAILED } from '../type/gallery'

const initialState = {
    loading : false,
    data : []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_GALLERY_DATA :
            return {
                ...state,
                loading : true
            }
        case SHOW_ALL_GALLERY_DATA :
            return {
                ...state,
                loading : false,
                data : action.payload
            }
        case CREATE_GALLERY :
            return {
                ...state,
                loading : true
            }
        case UPDATE_GALLERY : 
            return {
                ...state,
                loading : true
            }
        case DELETE_GALLERY : 
            return {
                ...state,
                loading : true
            }
        case REQUEST_GALLERY_COMPLETE :
            return {
                ...state,
                loading : false
            }
        case REQUEST_GALLERY_FAILED :
            return { 
                ...state,
                loading : false
            }
        default :
            return state
    }
}