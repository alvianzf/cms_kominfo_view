import { GET_LIST_OPD, SHOW_LIST_OPD, OPD_FAIL } from '../type/opd'

const initialState = {
    loading : false,
    data : []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_LIST_OPD :
            return {
                ...state,
                loading: true
            }
        case SHOW_LIST_OPD : 
            return {
                ...state,
                loading : false,
                data : action.payload
            }
        case OPD_FAIL :
            return {
                ...state,
                loading : false
            }
        default :
            return state
    }
}