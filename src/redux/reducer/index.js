import { combineReducers } from 'redux'
import { reducer as form } from 'redux-form'
import token from './token'
import login from './login'
import category from './category'
import article from './article'
import role from './role'
import user from './user'
import opd from './opd'
import news from './news'
import staff from './staff'
import comment from './comment'
import album from './album'
import gallery from './gallery'
import announcement from './announcement'

const IndexReducer = combineReducers ({
    form,
    token : token,
    login : login,
    category : category,
    article : article,
    role : role,
    user : user,
    opd : opd,
    news : news,
    staff : staff,
    comment : comment,
    album : album,
    gallery : gallery,
    announcement : announcement
})

export default IndexReducer