import { GET_STAFF_DATA, SHOW_STAFF_DATA, COMPLETE_STAFF_DATA,
    FAIL_STAFF_DATA  } from '../type/staff'

const initialState = {
    loading : false,
    data : []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_STAFF_DATA :
            return {
                ...state,
                loading: true
            }
        case SHOW_STAFF_DATA :
            return {
                ...state,
                loading: false,
                data : action.payload
            }
        case COMPLETE_STAFF_DATA :
            return {
                ...state,
                loading: false
            }
        case FAIL_STAFF_DATA :
            return {
                ...state,
                loading: false
            }
        default :
            return state
    }
}