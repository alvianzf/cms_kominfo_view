import { REQUEST_ALBUM, REQUEST_ALBUM_COMPLETE, REQUEST_ALBUM_FAILED,
    GET_LIST_ALBUM, GET_SINGLE_ALBUM, SHOW_LIST_ALBUM, SHOW_SINGLE_ALBUM,
    CREATE_ALBUM, UPDATE_ALBUM, DELETE_ALBUM } from '../type/album'

const initialState = {
    loading : true,
    data : [],
    single : null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case REQUEST_ALBUM :
            return {
                ...state,
                loading : true
            }
        case REQUEST_ALBUM_COMPLETE :
            return {
                ...state,
                loading : false
            }
        case REQUEST_ALBUM_FAILED :
            return {
                ...state,
                loading : false
            }
        case GET_LIST_ALBUM :
            return {
                ...state,
                loading : true
            }
        case GET_SINGLE_ALBUM :
            return {
                ...state,
                loading : true
            }
        case SHOW_LIST_ALBUM :
            return {
                ...state,
                data : action.payload
            }
        case SHOW_SINGLE_ALBUM :
            return {
                ...state,
                single : action.payload
            }
        case CREATE_ALBUM :
            return {
                ...state,
                loading : true
            }
        case UPDATE_ALBUM :
            return {
                ...state,
                loading : true
            }
        case DELETE_ALBUM :
            return {
                ...state,
                loading : true
            }
        default :
            return state
    }
}