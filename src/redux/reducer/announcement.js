import { GET_ALL_ANNOUNCEMENT, SHOW_ALL_ANNOUNCEMENT,
    GET_SINGLE_ANNOUNCEMENT, SHOW_SINGLE_ANNOUNCEMENT,
    SAVE_ANNOUNCEMENT,EDIT_ANNOUNCEMENT,
    DELETE_ANNOUNCEMENT,ANNOUNCEMENT_COMPLETE,
    ANNOUNCEMENT_FAILED } from '../type/announcement'

const initialState = {
    loading : true,
    data : [],
    single : null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_ANNOUNCEMENT :
            return {
                ...state,
                loading : true
            }
        case SHOW_ALL_ANNOUNCEMENT :
            return {
                ...state,
                loading : false,
                data : action.payload
            }
        case GET_SINGLE_ANNOUNCEMENT :
            return {
                ...state,
                loading : true
            }
        case SHOW_SINGLE_ANNOUNCEMENT : 
            return {
                ...state,
                loading : false,
                single : action.payload 
            }
        case SAVE_ANNOUNCEMENT :
            return {
                ...state,
                loading : true
            }
        case EDIT_ANNOUNCEMENT :
            return {
                ...state,
                loading : true
            }
        case DELETE_ANNOUNCEMENT :
            return {
                ...state,
                loading : true
            }
        case ANNOUNCEMENT_COMPLETE :
            return {
                ...state,
                loading : false
            }
        case ANNOUNCEMENT_FAILED :
            return {
                ...state,
                loading : false
            }
        default :
            return state
    }
}