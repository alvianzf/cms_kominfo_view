import { GET_ROLE , GET_ROLE_SUCCESS, ROLE_FAIL,
    SEND_ROLE, SEND_ROLE_SUCCESS, DELETE_ROLE, UPDATE_ROLE} from '../type/role'

const initialState = {
    loading : false,
    data : []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_ROLE :
            return {
                ...state,
                loading: true
            }
        case GET_ROLE_SUCCESS : 
            return {
                ...state,
                data : action.payload,
                loading : false
            }
        case ROLE_FAIL :
            return {
                ...initialState,
                loading : false
            }
        case SEND_ROLE :
            return {
                ...initialState,
                loading : true,
                payload : action.payload
            }
        case SEND_ROLE_SUCCESS :
            return {
                ...initialState,
                loading : false
            }
        case DELETE_ROLE :
            return {
                ...initialState,
                loading : true,
                payload : action.payload
            }
        case UPDATE_ROLE : 
            return {
                ...state,
                loading : true,
                payload : action.payload
            }
        default :
            return state
    }
}