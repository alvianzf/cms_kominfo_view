import { GET_DETAIL_USER , GET_LIST_USER, SHOW_DETAIL_USER,
    SHOW_LIST_USER, CREATE_USER, UPDATE_USER, USER_FAIL_FETCH, 
    USER_FETCH_COMPLETE, COMPLETE_USER, GET_ACTIVE_USER, SHOW_ACTIVE_USER } from '../type/user'

const initialState = {
    loading : false,
    detail : null,
    data : [],
    actived : null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_DETAIL_USER :
            return {
                ...state,
                loading: true
            }
        case GET_LIST_USER :
            return {
                ...state,
                loading : true
            }
        case SHOW_DETAIL_USER :
            return {
                ...state,
                loading : false,
                detail : action.payload
            }
        case SHOW_LIST_USER :
            return {
                ...state,
                loading : false,
                data : action.payload
            }
        case CREATE_USER :
            return {
                ...state,
                loading : true
            }
        case UPDATE_USER :
            return {
                ...state,
                loading : true
            }
        case USER_FAIL_FETCH :
            return {
                ...state,
                loading : false
            }
        case USER_FETCH_COMPLETE : 
            return {
                ...state,
                loading : false
            }
        case COMPLETE_USER :
            return {
                ...state,
                loading : false
            }
        case GET_ACTIVE_USER :
            return {
                ...state,
                loading : true
            }
        case SHOW_ACTIVE_USER :
            return {
                ...state,
                loading : false,
                actived : action.payload
            }
        default :
            return state
    }
}