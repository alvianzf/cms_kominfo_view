import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL} from '../type/login'

const initialState = {
    request : false,
    success : false,
    messages : null,
    error : false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_REQUEST :
            return {
                request : true,
                success : false,
                messages : null,
                error : false
            }
        case LOGIN_SUCCESS :
            return {
                request : false,
                success : true,
                messages : null,
                error : false
            }
        case LOGIN_FAIL :
            return {
                request : false,
                success : false,
                messages : action.messages,
                error : true
            }
        default :
            return state
    }
}