import { TOKEN_SET , TOKEN_UNSET } from '../type/token'

const initialState = {
    token : null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case TOKEN_SET :
            return {
                token : action.token
            }
        case TOKEN_UNSET :
            return {
                token : null
            }
        default :
            return state
    }
}