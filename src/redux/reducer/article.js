import { GET_LIST_ARTICLE,GET_SINGLE_ARTICLE,
    SHOW_LIST_ARTICLE,SHOW_SINGLE_ARTICLE,SAVE_ARTICLE,
    ARTICLE_SUCCESS,ARTICLE_FAIL,
    GET_LIST_GLOBAL_ARTICLE, SHOW_LIST_GLOBAL_ARTICLE, UPDATE_ARTICLE } from '../type/article'

const initialState = {
    loading : false,
    data : [],
    detail : null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_LIST_GLOBAL_ARTICLE :
            return {
                ...state,
                loading: true
            }
        case SHOW_LIST_GLOBAL_ARTICLE :
            return {
                ...state,
                loading: false,
                data : action.payload
            }
        case GET_LIST_ARTICLE :
            return {
                ...state,
                loading: true
            }
        case GET_SINGLE_ARTICLE :
            return {
                ...state,
                loading: true
            }
        case SHOW_LIST_ARTICLE : 
            return {
                ...state,
                data : action.payload,
                loading : false
            }
        case SHOW_SINGLE_ARTICLE :
            return {
                ...state,
                loading : false,
                detail : action.payload
            }
        case ARTICLE_FAIL :
            return {
                ...state,
                loading : false
            }
        case SAVE_ARTICLE :
            return {
                ...state,
                loading : true,
                payload : action.payload
            }
        case UPDATE_ARTICLE :
            return {
                ...state,
                loading : true,
                payload : action.payload
            }
        case ARTICLE_SUCCESS :
            return {
                ...state,
                loading : false
            }
        default :
            return state
    }
}