export const GET_LIST_COMMENT = 'GET_LIST_COMMENT'
export const SHOW_LIST_COMMENT = 'SHOW_LIST_COMMENT'
export const CREATE_COMMENT = 'CREATE_COMMENT'
export const DELETE_COMMENT = 'DELETE_COMMENT'
export const COMPLETE_FETCH_COMMENT = 'COMPLETE_FETCH_COMMENT'
export const FAIL_FETCH_COMMENT = 'FAIL_FETCH_COMMENT'