export const GET_NEWS_LIST_DATA = 'GET_NEWS_LIST_DATA'
export const GET_NEWS_SINGLE_DATA = 'GET_NEWS_SINGLE_DATA'
export const SHOW_NEWS_LIST_DATA = 'SHOW_NEWS_LIST_DATA'
export const SHOW_NEWS_SINGLE_DATA = 'SHOW_NEWS_SINGLE_DATA'
export const NEWS_SUCCESS = 'NEWS_SUCCESS'
export const NEWS_FAIL = 'NEWS_FAIL'
export const GET_NEWS_ALL_DATA = 'GET_NEWS_ALL_DATA'
export const SHOW_NEWS_ALL_DATA = 'SHOW_NEWS_ALL_DATA'