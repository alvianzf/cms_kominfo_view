import { GET_ALL_ANNOUNCEMENT, GET_SINGLE_ANNOUNCEMENT, 
    SAVE_ANNOUNCEMENT, EDIT_ANNOUNCEMENT, DELETE_ANNOUNCEMENT
} from '../type/announcement'

export const getAllAnnouncement = () => {
    return {
        type : GET_ALL_ANNOUNCEMENT,
        payload : null
    }
}

export const getSingleAnnouncement = data => {
    return {
        type : GET_SINGLE_ANNOUNCEMENT,
        payload : data
    }
}

export const saveAnnouncement = data => {
    return {
        type : SAVE_ANNOUNCEMENT,
        payload : data
    }
}

export const editAnnouncement = data => {
    return {
        type : EDIT_ANNOUNCEMENT,
        payload : data
    }
}

export const deleteAnnouncement = data => {
    return {
        type : DELETE_ANNOUNCEMENT,
        payload : data
    }
}