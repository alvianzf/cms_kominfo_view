import { GET_DETAIL_USER, GET_LIST_USER, 
    SHOW_DETAIL_USER, SHOW_LIST_USER, CREATE_USER, UPDATE_USER, GET_ACTIVE_USER } from '../type/user'

export const getDetailUser = () => {
    return {
        type : GET_DETAIL_USER,
        payload : null
    }
}

export const getListUser = () => {
    return {
        type : GET_LIST_USER,
        payload : null
    }
}

export const showDetailUser = (data) => {
    return {
        type : SHOW_DETAIL_USER,
        payload : data
    }
}

export const showListUser = (data) => {
    return {
        type : SHOW_LIST_USER,
        payload : data
    }
}

export const createUser = (data) => {
    return {
        type : CREATE_USER,
        payload : data
    }
}

export const updateUser = (data) => {
    return {
        type : UPDATE_USER,
        payload : data
    }
}

export const getActiveUser = () => {
    return {
        type : GET_ACTIVE_USER,
        payload : null
    }
}