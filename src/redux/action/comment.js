import { GET_LIST_COMMENT, CREATE_COMMENT, DELETE_COMMENT } from '../type/comment'

export const getListCommentData = (data) => {
    return {
        type : GET_LIST_COMMENT,
        payload : data
    }
}

export const createComment = (data) => {
    return {
        type : CREATE_COMMENT,
        payload : data
    }
}

export const deleteComment = (data) => {
    return {
        type : DELETE_COMMENT,
        payload : data
    }
}