import { GET_LIST_ALBUM, GET_SINGLE_ALBUM, SHOW_LIST_ALBUM, 
    SHOW_SINGLE_ALBUM, CREATE_ALBUM, UPDATE_ALBUM, DELETE_ALBUM
} from '../type/album'

export const getListAlbum = () => {
    return {
        type : GET_LIST_ALBUM,
        payload : null
    }
}

export const getSingleAlbum = (data) => {
    return {
        type : GET_SINGLE_ALBUM,
        payload : data
    }
}

export const showListAlbum = (data) => {
    return {
        type : SHOW_LIST_ALBUM,
        payload : data
    }
}

export const showSingleAlbum = (data) => {
    return {
        type : SHOW_SINGLE_ALBUM,
        payload : data
    }
}

export const createAlbum = (data) => {
    return {
        type : CREATE_ALBUM,
        payload : data
    }
}

export const updateAlbum = (data) => {
    return {
        type : UPDATE_ALBUM,
        payload : data
    }
}

export const deleteAlbum = (data) => {
    return {
        type : DELETE_ALBUM,
        payload : data
    }
}