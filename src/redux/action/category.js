import { FETCH_CATEGORY,SHOW_CATEGORY, FAIL_CATEGORY, 
        CREATE_CATEGORY, DELETE_CATEGORY, UPDATE_CATEGORY } from '../type/category'

export const fetchCategory = () => {
    return {
        type : FETCH_CATEGORY,
        payload : null
    }
}

export const createCategory = (data) => {
    return {
        type : CREATE_CATEGORY,
        payload : data
    }
}

export const updateCategory = (data) => {
    return {
        type : UPDATE_CATEGORY,
        payload : data
    }
}

export const deleteCategory = (data) => {
    return {
        type : DELETE_CATEGORY,
        payload : data
    }
}

export const showCategory = (data) => {
    return {
        type : SHOW_CATEGORY,
        payload : data
    }
}

export const failCategory = () => {
    return {
        type : FAIL_CATEGORY,
        payload : null
    }
}
