import { TOKEN_SET, TOKEN_UNSET } from '../type/token'

export const setToken = (payload) => {
    return {
        type : TOKEN_SET,
        token : payload
    }
}

export const unsetToken = () => {
    return {
        type : TOKEN_UNSET,
        token : null
    }
}