import { GET_STAFF_DATA, SHOW_STAFF_DATA } from '../type/staff'

export const getStaffData = () => {
    return {
        type : GET_STAFF_DATA,
        payload : null
    }
}

export const showStaffData = (data) => {
    return {
        type : SHOW_STAFF_DATA,
        payload : data
    }
}