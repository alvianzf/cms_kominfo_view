import { GET_LIST_ARTICLE, GET_SINGLE_ARTICLE, SAVE_ARTICLE, 
    UPDATE_ARTICLE,GET_LIST_GLOBAL_ARTICLE } from '../type/article'

export const getListArticle = () => {
    return {
        type : GET_LIST_ARTICLE,
        payload : null
    }
}

export const getListGlobalArticle = () => {
    return {
        type : GET_LIST_GLOBAL_ARTICLE,
        payload : null
    }
}

export const getSingleArticle = (data) => {
    return {
        type : GET_SINGLE_ARTICLE,
        payload : data
    }
}

export const saveArticleData = (data) => {
    return {
        type : SAVE_ARTICLE,
        payload : data
    }
}

export const updateArticleData = (data) => {
    return {
        type : UPDATE_ARTICLE,
        payload : data
    }
}


