import { LOGIN_REQUEST,LOGIN_FAIL } from '../type/login'

export const loginRequest = (payload) => {
    return {
        type : LOGIN_REQUEST,
        payload : payload
    }
}

export const loginFail = (value) => {
    return {
        type : LOGIN_FAIL,
        messages : value
    }
}
