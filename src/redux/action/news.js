import { GET_NEWS_LIST_DATA, GET_NEWS_SINGLE_DATA,
    GET_NEWS_ALL_DATA } from '../type/news'

export const getNewsListData = () => {
    return {
        type : GET_NEWS_LIST_DATA,
        payload : null
    }
}

export const getNewsSingleData = (data) => {
    return {
        type : GET_NEWS_SINGLE_DATA,
        payload : data
    }
}

export const getNewsAllData = () => {
    return {
        type : GET_NEWS_ALL_DATA,
        payload : null
    }
}