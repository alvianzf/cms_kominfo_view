import { GET_LIST_OPD } from '../type/opd'

export const getListOpd = () => {
    return {
        type : GET_LIST_OPD,
        payload : null
    }
}