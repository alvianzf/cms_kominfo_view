import { GET_ROLE, GET_ROLE_SUCCESS, SEND_ROLE, DELETE_ROLE, UPDATE_ROLE } from '../type/role'

export const getRole = () => {
    return {
        type : GET_ROLE,
        payload : null
    }
}

export const getRoleSuccess = (data) => {
    return {
        type : GET_ROLE_SUCCESS,
        payload : data
    }
}

export const sendRole = (data) => {
    return {
        type : SEND_ROLE,
        payload : data
    }
}

export const deleteRole = (data) => {
    return {
        type : DELETE_ROLE,
        payload : data
    }
}

export const updateRole = (data) => {
    return {
        type : UPDATE_ROLE,
        payload : data
    }
}