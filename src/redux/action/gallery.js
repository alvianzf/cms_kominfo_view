import { GET_ALL_GALLERY_DATA , CREATE_GALLERY, UPDATE_GALLERY, 
    DELETE_GALLERY } from '../type/gallery'

export const getAllGallery = () => {
    return {
        type : GET_ALL_GALLERY_DATA,
        payload : null
    }
}

export const createGallery = data => {
    return {
        type : CREATE_GALLERY,
        payload : data
    }
}

export const updateGallery = data => {
    return {
        type : UPDATE_GALLERY,
        payload : data
    }
}

export const deleteGallery = data => {
    return {
        type : DELETE_GALLERY,
        payload : data
    }
}