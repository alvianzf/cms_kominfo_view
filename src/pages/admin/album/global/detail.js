import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { getSingleAlbum, updateAlbum, deleteAlbum } from '../../../../redux/action/album'
import Loading from '../../../../component/loading'

class Detail extends Component {
    constructor(props){
        super(props)
        this.state = {
            title : "",
            description : ""
        }
    }
    componentDidMount()
    {
        this.props.getSingleAlbumData(this.props.match.params.id)
    }

    componentWillReceiveProps()
    {
        this.setState({
            title : this.props.album.single ? this.props.album.single.title : "",
            description : this.props.album.single ? this.props.album.single.description : "",
        })
    }
    
    onChangeTitle = (e) => {
        this.setState({
            title : e.target.value
        })
    }
    onChangeDescription = (e) => {
        this.setState({
            description : e.target.value
        })
    }

    onUpdateData = () => {
        const data = {
            title : this.state.title,
            description : this.state.description
        }

        const slug = {
            slug : this.props.match.params.id
        }

        const sendData = {
            data : data,
            slug : slug
        }

        this.props.updateAlbumData(sendData)
    }

    onDeleteData = () => {
        this.props.deleteAlbumData(this.props.match.params.id)
    }

    render() {
        return (
            <div>
                {this.props.album.loading ? (
                    <Loading />
                ) : (
                    <div>
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">{this.props.match.params.id}</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li>
                                        <Link to="/admin/album/opd"><i className="fa-image"></i>Album Global</Link>
                                    </li>
                                    <li className="active">
                                        <strong>{this.props.match.params.id}</strong>
                                    </li>
                                </ol>        
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Album Data
                                        </div>
                                    </div>
                                    <div className="panel-body">
                                        <form id="album">
                                            <div className="form-group">
                                                <label>Judul Album:</label>
                                                <input type="text" name="title" className="form-control" defaultValue={this.state.title} onChange={this.onChangeTitle.bind(this)}/>
                                            </div>
                                            <div className="form-group">
                                                <label>Descripsi Album:</label>
                                                <textarea name="description" className="form-control" rows={5} onChange={this.onChangeDescription.bind(this)} value={this.state.description}/>
                                            </div>
                                            <div className="form-group">
                                                <button type="button" className="btn btn-info btn-single" onClick={this.onUpdateData.bind(this)}>Update Album</button>
                                                <button type="button" className="btn btn-danger btn-single" onClick={this.onDeleteData.bind(this)}>Hapus Album</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}


const mapStateToProps = state => ({
    album : state.album,
})

const mapDispatchToProps = dispatch => {
    return {
        getSingleAlbumData : data => dispatch(getSingleAlbum(data)),
        updateAlbumData : data => dispatch(updateAlbum(data)),
        deleteAlbumData : data => dispatch(deleteAlbum(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail)