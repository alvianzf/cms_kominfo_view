import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { getSingleAlbum, updateAlbum, deleteAlbum } from '../../../../redux/action/album'
import { getListOpd } from '../../../../redux/action/opd'
import Loading from '../../../../component/loading'
import classNames from 'classnames'
import Dropzone from 'react-dropzone'
import { createGallery } from '../../../../redux/action/gallery'

class Detail extends Component {
    constructor(props){
        super(props)
        this.state = {
            opd : "",
            selectedOpd : "",
            title : "",
            description : ""
        }
    }
    componentDidMount()
    {
        this.props.getSingleAlbumData(this.props.match.params.id)
        this.props.getOpdListData()
    }

    componentWillReceiveProps()
    {
        this.setState({
            opd : this.props.opd ? this.props.opd.data : "",
            selectedOpd : this.props.album.single ? this.props.album.single.opd : "",
            title : this.props.album.single ? this.props.album.single.title : "",
            description : this.props.album.single ? this.props.album.single.description : "",
        })
    }
    
    onChangeOpd = (e) => {
        this.setState({
            opd : e.target.value
        })
    }
    onChangeTitle = (e) => {
        this.setState({
            title : e.target.value
        })
    }
    onChangeDescription = (e) => {
        this.setState({
            description : e.target.value
        })
    }

    onUpdateData = () => {
        const data = {
            title : this.state.title,
            description : this.state.description,
            opd : this.state.opd
        }

        const slug = {
            slug : this.props.match.params.id
        }

        const sendData = {
            data : data,
            slug : slug
        }

        this.props.updateAlbumData(sendData)
    }

    onDeleteData = () => {
        this.props.deleteAlbumData(this.props.match.params.id)
    }

    onDrop = files => {
        // console.log(files)
        files.forEach(file => {
            const data = {
                photo : file,
                album : this.props.album.single._id
            }
            this.props.createGalleryData(data)
        });
    }

    render() {
        const opdOption = this.props.opd.data.map(opd => (
            <option key={opd.id_opd} value={opd.link_opd} > {opd.nama_opd} </option>
        ))
        console.log(this.props.album.single)
        return (
            <div>
                {this.props.opd.loading ? (
                    <Loading />
                ) : (
                    <div>
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">{this.props.match.params.id}</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li>
                                        <Link to="/admin/album/opd"><i className="fa-image"></i>Album Opd</Link>
                                    </li>
                                    <li className="active">
                                        <strong>{this.props.match.params.id}</strong>
                                    </li>
                                </ol>        
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Album Data
                                        </div>
                                    </div>
                                    <div className="panel-body">
                                        <form id="album">
                                            <div className="form-group">
                                                <label>Judul Album:</label>
                                                <input type="text" name="title" className="form-control" defaultValue={this.state.title} onChange={this.onChangeTitle.bind(this)}/>
                                            </div>
                                            <div className="form-group">
                                                <label>Descripsi Album:</label>
                                                <textarea name="description" className="form-control" rows={5} onChange={this.onChangeDescription.bind(this)} value={this.state.description}/>
                                            </div>
                                            <div className="form-group">
                                                <label>Pilih OPD :</label>
                                                <select name="opd" className="form-control" onChange={this.onChangeOpd.bind(this)}
                                                    value={this.state.selectedOpd}>
                                                    <option>Pilih Instansi </option>
                                                    {opdOption}
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <button type="button" className="btn btn-info btn-single" onClick={this.onUpdateData.bind(this)}>Update Album</button>
                                                <button type="button" className="btn btn-danger btn-single" onClick={this.onDeleteData.bind(this)}>Hapus Album</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Gallery
                                        </div>
                                    </div>
                                    <div className="panel-body">
                                        <Dropzone accept="image/jpeg, image/png" onDrop={this.onDrop} multiple>
                                            {({getRootProps, getInputProps, isDragActive}) => {
                                            return (
                                                <div
                                                {...getRootProps()}
                                                className={classNames('dropzone', {'dropzone--isActive': isDragActive})}
                                                >
                                                <input {...getInputProps()} />
                                                {
                                                    isDragActive ?
                                                    <p>Drop files here...</p> :
                                                    <div className="dz-default dz-message"><span>Drop files here to upload</span></div>
                                                }
                                                </div>
                                            )
                                            }}
                                        </Dropzone>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}


const mapStateToProps = state => ({
    album : state.album,
    opd : state.opd,
    gallery : state.gallery
})

const mapDispatchToProps = dispatch => {
    return {
        getSingleAlbumData : data => dispatch(getSingleAlbum(data)),
        getOpdListData : () => dispatch(getListOpd()),
        updateAlbumData : data => dispatch(updateAlbum(data)),
        deleteAlbumData : data => dispatch(deleteAlbum(data)),
        createGalleryData : data => dispatch(createGallery(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail)