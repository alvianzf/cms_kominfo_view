import React, { Component } from 'react'
import { reduxForm , Field } from 'redux-form'
import { connect } from 'react-redux'
import { getListAlbum, createAlbum } from '../../../../redux/action/album'
import { getListOpd } from '../../../../redux/action/opd'
import Loading from '../../../../component/loading'
import { Link } from 'react-router-dom'

class Index extends Component {
    componentDidMount()
    {
        this.props.getListAlbumData()
        this.props.getOpdListData()
    }

    onPressToSave = (value) => {
        this.props.saveAlbumData(JSON.stringify(value))
        this.props.reset()
    }

    render() {

        const { handleSubmit } = this.props
        const opdOption = this.props.opd.data.map(opd => (
            <option key={opd.id_opd} value={opd.link_opd} > {opd.nama_opd} </option>
        ))

        const albumList = this.props.album.data.map( album => (
            <div key={album._id} className="col-md-3 col-sm-4 col-xs-6">
                <Link to={`/admin/album/opd/${album.slug}`}>
                    <div className="album-image">
                        {album.gallery.length === 0 ? (
                            <img src="https://via.placeholder.com/150x150" className="img-responsive" alt=""/>
                        ) : (
                            <img src="https://via.placeholder.com/250x250" className="img-responsive" alt=""/>
                        )} 
                        
                        <span>{album.title}</span>
                    </div>
                </Link>
            </div>
        ))
        return (
            <div>
                {this.props.opd.loading ? (
                    <Loading />
                ): (
                    <div>
                        <section className="gallery-env">
                            <div className="row">
                                <div className="col-md-4 gallery-left">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <div className="panel-title">
                                                Form Album Opd
                                            </div>
                                        </div>
                                        <div className="panel-body">
                                            <form id="album" onSubmit={handleSubmit(this.onPressToSave.bind(this))}>
                                                <div className="form-group">
                                                    <label> Title Album : </label>
                                                    <Field name="title" type="text" id="title"
                                                        className="form-control"
                                                        component="input"
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label>Deskripsi Album : </label>
                                                    <Field name="description" type="text" id="description"
                                                        className="form-control"
                                                        component="textarea" rows={5}
                                                        placeholder = "Tulis Deskripsi Singkat Disini"
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label>Pilih OPD :</label>
                                                    <Field name="opd" className="form-control"
                                                        component="select">
                                                        <option>Pilih Instansi </option>
                                                        {opdOption}
                                                    </Field>
                                                </div>
                                                <div className="form-group">
                                                    <button type="submit" className="btn btn-info btn-single pull-right">Tambah Album</button>
                                                </div>  
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-8 gallery-right">
                                    <div className="album-header">
                                        <h2>Album Data</h2>
                                    </div>
                                    <div className="album-images row">
                                        {albumList}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                )}
                
            </div>
        )
    }
}

const mapStateToProps = state => ({
    album : state.album,
    opd : state.opd
})

const mapDispatchToProps = dispatch => {
    return {
        getListAlbumData : () => dispatch(getListAlbum()),
        saveAlbumData : data => dispatch(createAlbum(data)),
        getOpdListData : () => dispatch(getListOpd())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: 'album'})(Index))