import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { updateArticleData } from '../../../redux/action/article'
import Loading from '../../../component/loading'
import { fetchCategory } from '../../../redux/action/category'
import { getListOpd } from '../../../redux/action/opd'
import { getSingleArticle } from '../../../redux/action/article'
import { EditorState, convertToRaw, ContentState, convertFromHTML} from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { reduxForm , Field } from 'redux-form'
import { getListCommentData } from '../../../redux/action/comment'
import ReactTable from 'react-table'
import "react-table/react-table.css"

class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            body : "",
            editorState: EditorState.createEmpty(),
            thumbnail : ""
        }
    }

    componentDidMount = () => {
        this.props.getCategoryData()
        this.props.getOpdListData()
        this.props.getSingleArticleData(this.props.match.params.id)
        this.props.getCommentData(this.props.match.params.id)
    }
    
    componentDidUpdate = () => {
        this.handleInitialize()
        
    }

    componentWillReceiveProps = () => {
        this.setState({
            editorState: EditorState.createWithContent(
                ContentState.createFromBlockArray(
                    convertFromHTML(this.props.article.detail ? this.props.article.detail.body : "<p>No Content Yet</p>")
                )
            ),
        })
    }

    handleInitialize() {
        const initData = {
          "title": this.props.article.detail ? this.props.article.detail.title : null,
          "description" : this.props.article.detail ? this.props.article.detail.description : null,
          "category" : this.props.article.detail ? this.props.article.detail.category : null,
          "opd" : this.props.article.detail ? this.props.article.detail.opd : null
        };
    
        this.props.initialize(initData);
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
        this.setState({
            body : draftToHtml(convertToRaw(editorState.getCurrentContent()))
        })
    };

    setThumbnail = (e) => {
        this.setState({
            thumbnail : e.target.files[0]
        })
    }

    onClickPublish = (value) => {
        const data = {
            ...value,
            body : this.state.body,
            thumbnail : this.state.thumbnail,
            slug : this.props.article.detail ? this.props.article.detail.slug : null,
            status : "Publish"
        }
        this.props.updateArticle(data)
    }

    onClickSave = (value) => {
        const data = {
            ...value,
            body : this.state.body,
            thumbnail : this.state.thumbnail,
            status : "Not Publish"
        }
        this.props.saveArticle(data)
    }
    render() {

        const { editorState } = this.state;
        const { handleSubmit } = this.props
        const categoryOption = this.props.category.data.map( category => (
            <option key={category._id} value={category._id} > {category.name} </option>
        ))
        const opdOption = this.props.opd.data.map(opd => (
            <option key={opd.id_opd} value={opd.link_opd} > {opd.nama_opd} </option>
        ))
        return (
            <div>
                { this.props.article.loading ? (
                    <Loading />
                ): (
                    <div>
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">Artikel</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li>
                                        <Link to="/admin/article"><i className="fa-news"></i>Artikel</Link>
                                    </li>
                                    <li className="active">
                                        <strong>Artikel</strong>
                                    </li>
                                </ol>        
                            </div>
                        </div>
                        <form id="article">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <div className="panel-title">
                                                Artikel Baru
                                            </div>
                                        </div>
                                        <div className="panel-body">
                                            <div className="form-group">
                                                <label>Judul Artikel:</label>
                                                <Field name="title" type="text" id="title"
                                                    className="form-control"
                                                    component="input"
                                                    placeholder = "Tulis Judul Artikel Disini"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label>Descripsi Singkat Artikel:</label>
                                                <Field name="description" type="text" id="description"
                                                    className="form-control"
                                                    component="textarea" rows={5}
                                                    placeholder = "Tulis Deskripsi Singkat Disini"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label>Tulis Artikel:</label>
                                                <Editor
                                                    editorState={editorState}
                                                    toolbarClassName="toolbarClassName"
                                                    wrapperClassName="demo-wrapper"
                                                    editorClassName="demo-editor"
                                                    onEditorStateChange={this.onEditorStateChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <div className="panel-title">
                                                Atribute Artikel
                                            </div>
                                        </div>
                                        <div className="panel-body">
                                            <div className="row">
                                                <div className="col-md-3">
                                                    <div className="form-group">
                                                        <label>Kategori Artikel *:</label>
                                                        <Field name="category"
                                                            className="form-control"
                                                            component="select"
                                                        >
                                                            <option>Pilih Kategori</option>
                                                            {categoryOption}
                                                        </Field>
                                                    </div>
                                                </div> 
                                                <div className="col-md-6">
                                                    <div className="form-group">
                                                        <label>Thumbnail Artikel :</label>
                                                        <input type="file" name="thumbnail" className="form-control" onChange={this.setThumbnail} />
                                                    </div>
                                                </div>  
                                                <div className="col-md-3">
                                                    <div className="form-group">
                                                        <label>Instansi Artikel *:</label>
                                                        <Field name="opd" className="form-control"
                                                            component="select">
                                                            <option>Pilih Instansi </option>
                                                            {opdOption}
                                                        </Field>
                                                    </div>
                                                </div>  
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <button type="button" className="btn btn-success" onClick={ handleSubmit(this.onClickPublish.bind(this)) } >Publish</button>
                                                        <button type="button" className="btn btn-warning" onClick={ handleSubmit(this.onClickSave.bind(this)) }>Simpan</button>
                                                        <button type="button" className="btn btn-danger">Delete</button>
                                                        <button type="button" className="btn btn-primary">Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Data Komentar
                                        </div>
                                    </div>
                                    <ReactTable
                                        data={this.props.comment.data}
                                        columns={[
                                            {
                                                Header: "No",
                                                accessor: "id",
                                                maxWidth : 50,
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Komentar",
                                                accessor : "body",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Aksi",
                                                accessor : "action",
                                                maxWidth : 100,
                                                sortable: false,
                                                filterable: false,
                                                style : {
                                                    "textAlign" : "center"
                                                },
                                                Cell: value => (
                                                    <div className="vertical-top">
                                                        
                                                    </div>
                                                )
                                            }
                                        ]}
                                        defaultPageSize={10}
                                        className="table table-striped"
                                        filterable
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                )}
                
            </div>
        )
    }
}
const mapStateToProps = state => ({
    article : state.article,
    category : state.category,
    opd : state.opd,
    comment : state.comment
})

const mapDispatchToProps = dispatch => {
    return {
        getCategoryData : () => dispatch(fetchCategory()),
        updateArticle : data => dispatch(updateArticleData(data)),
        getOpdListData : () => dispatch(getListOpd()),
        getSingleArticleData : data => dispatch(getSingleArticle(data)),
        getCommentData : (data) => dispatch(getListCommentData(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: 'article'})(Edit))