import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { saveArticleData } from '../../../redux/action/article'
import Loading from '../../../component/loading'
import { fetchCategory } from '../../../redux/action/category'
import { getListOpd } from '../../../redux/action/opd'
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { reduxForm , Field } from 'redux-form'

class New extends Component {
    constructor(props) {
        super(props);
        this.state = {
            body : "",
            editorState: EditorState.createEmpty(),
            thumbnail : ""
        }
    }

    componentDidMount = () => {
        this.props.getCategoryData()
        this.props.getOpdListData()
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
        this.setState({
            body : draftToHtml(convertToRaw(editorState.getCurrentContent()))
        })
    };

    setThumbnail = (e) => {
        this.setState({
            thumbnail : e.target.files[0]
        })
    }

    onClickPublish = (value) => {
        const data = {
            ...value,
            body : this.state.body,
            thumbnail : this.state.thumbnail,
            status : "Publish"
        }
        this.props.saveArticle(data)
    }

    onClickSave = (value) => {
        const data = {
            ...value,
            body : this.state.body,
            thumbnail : this.state.thumbnail,
            status : "Not Publish"
        }
        this.props.saveArticle(data)
    }

    handleInitialize() {
        const role = localStorage.getItem('role');
        if(role != 0) {
            const initData = {
              "opd" : process.env.REACT_APP_OPD_ID
            };
        
            this.props.initialize(initData);
        }
    }

    render() {
        const role = localStorage.getItem('role');
        console.log(this.props.opd)
        const { editorState } = this.state;
        const { handleSubmit } = this.props
        const categoryOption = this.props.category.data.map( category => (
            <option key={category._id} value={category._id} > {category.name} </option>
        ))
        const opdOption = this.props.opd.data.map(opd => (
            <option key={opd.id_opd} value={opd.link_opd} > {opd.nama_opd} </option>
        ))
        return (
            <div>
                { this.props.opd.loading ? (
                    <Loading />
                ): (
                    <div>
                    
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">Artikel</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li className="active">
                                        <strong>Artikel</strong>
                                    </li>
                                </ol>        
                            </div>
                        </div>
                        <form id="article">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <div className="panel-title">
                                                Artikel Baru
                                            </div>
                                        </div>
                                        <div className="panel-body">
                                            <div className="form-group">
                                                <label>Judul Artikel:</label>
                                                <Field name="title" type="text" id="title"
                                                    className="form-control"
                                                    component="input"
                                                    placeholder = "Tulis Judul Artikel Disini"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label>Descripsi Singkat Artikel:</label>
                                                <Field name="description" type="text" id="description"
                                                    className="form-control"
                                                    component="textarea" rows={5}
                                                    placeholder = "Tulis Deskripsi Singkat Disini"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label>Tulis Artikel:</label>
                                                <Editor
                                                    editorState={editorState}
                                                    toolbarClassName="toolbarClassName"
                                                    wrapperClassName="demo-wrapper"
                                                    editorClassName="demo-editor"
                                                    onEditorStateChange={this.onEditorStateChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <div className="panel-title">
                                                Atribute Artikel
                                            </div>
                                        </div>
                                        <div className="panel-body">
                                            <div className="row">
                                                <div className="col-md-3">
                                                    <div className="form-group">
                                                        <label>Kategori Artikel *:</label>
                                                        <Field name="category"
                                                            className="form-control"
                                                            component="select"
                                                        >
                                                            <option>Pilih Kategori</option>
                                                            {categoryOption}
                                                        </Field>
                                                    </div>
                                                </div> 
                                                <div className="col-md-6">
                                                    <div className="form-group">
                                                        <label>Thumbnail Artikel :</label>
                                                        <input type="file" name="thumbnail" className="form-control" onChange={this.setThumbnail} />
                                                    </div>
                                                </div>  
                                                <div className="col-md-3">
                                                    <div className="form-group">
                                                        <label>Instansi Artikel *:</label>
                                                        <Field name="opd" className="form-control"
                                                            component="select">
                                                                <option>Pilih Instansi </option>
                                                                {opdOption}
                                                        </Field>
                                                        
                                                    </div>
                                                </div>  
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <button type="button" className="btn btn-success" onClick={ handleSubmit(this.onClickPublish.bind(this)) } >Publish</button>
                                                        <button type="button" className="btn btn-warning" onClick={ handleSubmit(this.onClickSave.bind(this)) }>Simpan</button>
                                                        <button type="button" className="btn btn-danger">Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                )}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    article : state.article,
    category : state.category,
    opd : state.opd
})

const mapDispatchToProps = dispatch => {
    return {
        getCategoryData : () => dispatch(fetchCategory()),
        saveArticle : data => dispatch(saveArticleData(data)),
        getOpdListData : () => dispatch(getListOpd())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: 'article'})(New))