import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ReactTable from 'react-table'
import "react-table/react-table.css"
import { connect } from 'react-redux'
import { getListArticle } from '../../../redux/action/article'
import Loading from '../../../component/loading'
import history from '../../../history'

class Index extends Component {
    componentDidMount() {
        this.props.getListArticleData()
    }

    render() {
        return (
            <div> 
                { this.props.article.loading ? (
                    <Loading />
                ): (
                    <div>
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">Artikel</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li className="active">
                                        <strong>Artikel</strong>
                                    </li>
                                </ol>
                                        
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Data artikel
                                        </div>
                                        <div className="panel-options">
                                            <Link to="/admin/article/new" className="btn btn-secondary btn-icon btn-icon-standalone">
                                                <i className="fa-plus"></i>
                                                <span>Tambah Artikel</span>
                                            </Link>
                                        </div>
                                    </div>
                                    <ReactTable
                                        data={this.props.article.data}
                                        columns={[
                                            {
                                                Header: "#",
                                                accessor: "@",
                                                maxWidth : 50,
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Judul Artikel",
                                                accessor : "title",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Kategori Artikel",
                                                accessor : "category[0].name",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Author Artikel",
                                                accessor : "author.name",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Status",
                                                accessor : "status",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Aksi",
                                                accessor : "action",
                                                maxWidth : 100,
                                                sortable: false,
                                                filterable: false,
                                                style : {
                                                    "textAlign" : "center"
                                                },
                                                Cell: value => (
                                                    <div className="vertical-top">
                                                        <button onClick={() => history.push('/admin/article/edit/'+value.original.slug) } style={{ marginBottom : 0 }} className="btn btn-sm btn-icon btn-blue">
                                                            <i className="fa-eye"></i>
                                                        </button>
                                                    </div>
                                                )
                                            }
                                        ]}
                                        defaultPageSize={10}
                                        className="table table-striped table-bordered"
                                        filterable
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                )}  
                

            </div>
        )
    }
}

const mapStateToProps = state => ({
    article : state.article
})

const mapDispatchToProps = dispatch => {
    return {
        getListArticleData : () => dispatch(getListArticle())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)