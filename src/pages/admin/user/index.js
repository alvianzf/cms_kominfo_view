import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { reduxForm , Field } from 'redux-form'
import ReactTable from 'react-table'
import "react-table/react-table.css"
import { connect } from 'react-redux'
import { getListUser, createUser } from '../../../redux/action/user'
import { getStaffData } from '../../../redux/action/staff'
import { getRole } from '../../../redux/action/role'
import Loading from '../../../component/loading'
import Select from 'react-select'

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            edit : false,
            idPeg : null,
            name : null,
            selectedOption : null,
            avatar : null
        }
    }

    componentDidMount() {
        this.props.getListUserData()
        this.props.getListRoleData()
        this.props.getListStaffData()
    }

    handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        console.log(`Option selected:`, selectedOption);
    }

    setAvatar = (e) => {
        this.setState({
            avatar : e.target.files[0]
        })
    }

    onSave = (value) => {
        const data = {
            ...value,
            name : this.state.selectedOption.label,
            idPeg : this.state.selectedOption.value,
            avatar : this.state.avatar
        }
        this.props.createSaveData(data)
        this.props.reset()
    }

    render() {
        const { handleSubmit } = this.props
        const roleList = this.props.role.data.map( role => (
            <option key={role._id} value={role._id}>{role.name}</option>
        ))
        const options = []
        this.props.staff.data.forEach( staff => {
            options.push(
                {
                    value : staff.id_peg,
                    label : staff.nama_gelar
                }
            )
        })
        return (
            <div>
                { this.props.staff.loading ? (
                    <Loading />
                ) : (
                    <div>   
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">Daftar Pengguna</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li className="active">
                                        <strong>Pengguna</strong>
                                    </li>
                                </ol>
                                        
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Form Pengguna
                                        </div>
                                    </div>
                                    <div className="panel-body">
                                        { this.state.edit ? (
                                            <form id="user">

                                            </form>
                                        ) : (
                                            <form id="user" onSubmit={handleSubmit(this.onSave)}>
                                                <div className="form-group">
                                                <label> Daftar Pegawai </label>
                                                <Select
                                                    value={this.state.selectedOption}
                                                    options={options}
                                                    onChange={this.handleChange}
                                                />
                                                </div>
                                                <div className="form-group">
                                                    <label> Email : </label>
                                                    <Field name="email" type="email" id="email"
                                                        className="form-control"
                                                        component="input"
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label> Password : </label>
                                                    <Field name="password" type="password" id="password"
                                                        className="form-control"
                                                        component="input"
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label>Avatar :</label>
                                                    <input type="file" name="thumbnail" className="form-control" onChange={this.setAvatar} />
                                                </div>
                                                <div className="form-group">
                                                    <label> Pilih Hak Akses : </label>
                                                    <Field name="role"
                                                        className="form-control"
                                                        component="select"
                                                    >
                                                        <option>Pilih Hak Akses</option>
                                                        {roleList}
                                                    </Field>
                                                </div>
                                                <div className="form-group">
                                                    <button type="submit" className="btn btn-info btn-single pull-right">Tambah Pengguna</button>
                                                </div>
                                            </form>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Data Daftar Pengguna
                                        </div>
                                    </div>
                                    <ReactTable
                                        data={this.props.user.data}
                                        columns={[
                                            {
                                                Header: "#",
                                                accessor: "id",
                                                maxWidth : 50,
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Id Pegawai",
                                                accessor : "idPeg",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Email",
                                                accessor : "email",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Nama Lengkap",
                                                accessor : "name",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Hak Akses",
                                                accessor : "role",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            }
                                        ]}
                                        defaultPageSize={10}
                                        className="table table-striped table-bordered"
                                        filterable
                                    />
                                </div>
                            </div>
                        </div>

                    </div>
                )}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user : state.user,
    role : state.role,
    staff : state.staff
})

const mapDispatchToProps = dispatch => {
    return {
        getListUserData : () => dispatch(getListUser()),
        getListRoleData : () => dispatch(getRole()),
        getListStaffData : () => dispatch(getStaffData()),
        createSaveData : (data) => dispatch(createUser(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: 'user'})(Index))