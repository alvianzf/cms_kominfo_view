import React, { Component } from 'react'
import { Link } from 'react-router-dom';

class Index extends Component {
    constructor(props)
    {
        super(props)
        document.body.classList.remove('login-page');
    }

    render() {
        return (
            <div>   
                <div className="page-title">
                    <div className="title-env">
                        <h1 className="title">Beranda</h1>
                    </div>
                    <div className="breadcrumb-env">
                        <ol className="breadcrumb bc-1">
                            <li>
                                <Link to="/"><i className="fa-home"></i>Home</Link>
                            </li>
                            <li className="active">
                                <strong>Beranda</strong>
                            </li>
                        </ol>
                                
                    </div>
                </div>
                <h5> Everything Goes Here </h5>
            </div>
        )
    }
}

export default Index