import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ReactTable from 'react-table'
import "react-table/react-table.css"
import history from '../../../history'
import { connect } from 'react-redux'
import { getAllAnnouncement } from '../../../redux/action/announcement'
import Loading from '../../../component/loading'

class Index extends Component {

    componentDidMount()
    {
        this.props.getListAllAnnouncement()
    }

    render() {
        console.log(this.props.announcement)
        return (
            <div> 
                { this.props.announcement.loading ? (
                    <Loading />
                ): (
                    <div>
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">Pengumuman Dinas</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li className="active">
                                        <strong>Pengumuman Dinas</strong>
                                    </li>
                                </ol>
                                        
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Pengumuman Dinas
                                        </div>
                                        <div className="panel-options">
                                            <Link to="/admin/announcement/new" className="btn btn-secondary btn-icon btn-icon-standalone">
                                                <i className="fa-plus"></i>
                                                <span>Tambah Pengumuman</span>
                                            </Link>
                                        </div>
                                    </div>
                                    <ReactTable
                                        data={this.props.announcement.data}
                                        columns={[
                                            {
                                                Header: "#",
                                                accessor: "@",
                                                maxWidth : 50,
                                                sortable: false,
                                                filterable: false,
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Judul",
                                                accessor : "title",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Aksi",
                                                accessor : "action",
                                                maxWidth : 100,
                                                sortable: false,
                                                filterable: false,
                                                style : {
                                                    "textAlign" : "center"
                                                },
                                                Cell: value => (
                                                    <div className="vertical-top">
                                                        <button onClick={() => history.push('/admin/announcement/edit/'+value.original.slug) } style={{ marginBottom : 0 }} className="btn btn-sm btn-icon btn-blue">
                                                            <i className="fa-eye"></i>
                                                        </button>
                                                    </div>
                                                )
                                            }
                                        ]}
                                        defaultPageSize={10}
                                        className="table table-striped table-bordered"
                                        filterable
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    announcement : state.announcement
})

const mapDispatchToProps = dispatch => {
    return {
        getListAllAnnouncement : () => dispatch(getAllAnnouncement())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Index)