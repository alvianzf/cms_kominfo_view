import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import Loading from '../../../component/loading'
import { fetchCategory } from '../../../redux/action/category'
import { getListOpd } from '../../../redux/action/opd'
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { reduxForm , Field } from 'redux-form'
import { saveAnnouncement } from '../../../redux/action/announcement'

class New extends Component {
    constructor(props) {
        super(props);
        this.state = {
            body : "",
            editorState: EditorState.createEmpty(),
            thumbnail : ""
        }
    }

    componentDidMount = () => {
        this.props.getCategoryData()
        this.props.getOpdListData()
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
        this.setState({
            body : draftToHtml(convertToRaw(editorState.getCurrentContent()))
        })
    };

    setThumbnail = (e) => {
        this.setState({
            thumbnail : e.target.files[0]
        })
    }

    onClickSave = (value) => {
        const data = {
            ...value,
            body : this.state.body,
            thumbnail : this.state.thumbnail,
            status : "Publish"
        }
        this.props.saveAnnouncementData(data)
    }

    render() {
        const { editorState } = this.state;
        const { handleSubmit } = this.props
        const categoryOption = this.props.category.data.map( category => (
            <option key={category._id} value={category._id} > {category.name} </option>
        ))
        const opdOption = this.props.opd.data.map(opd => (
            <option key={opd.id_opd} value={opd.link_opd} > {opd.nama_opd} </option>
        ))
        return (
            <div>
                { this.props.opd.loading ? (
                    <Loading />
                ): (
                    <div>
                    
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">Pengumuman</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li className="active">
                                        <strong>Pengumuman</strong>
                                    </li>
                                </ol>        
                            </div>
                        </div>
                        <form id="announcement">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <div className="panel-title">
                                                Pengumuman Baru
                                            </div>
                                        </div>
                                        <div className="panel-body">
                                            <div className="form-group">
                                                <label>Judul Pengumuman:</label>
                                                <Field name="title" type="text" id="title"
                                                    className="form-control"
                                                    component="input"
                                                    placeholder = "Tulis Judul Pengumuman Disini"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label>Descripsi Singkat Pengumuman:</label>
                                                <Field name="description" type="text" id="description"
                                                    className="form-control"
                                                    component="textarea" rows={5}
                                                    placeholder = "Tulis Pengumuman Singkat Disini"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label>Tulis Pengumuman:</label>
                                                <Editor
                                                    editorState={editorState}
                                                    toolbarClassName="toolbarClassName"
                                                    wrapperClassName="demo-wrapper"
                                                    editorClassName="demo-editor"
                                                    onEditorStateChange={this.onEditorStateChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <div className="panel-title">
                                                Atribute Pengumuman
                                            </div>
                                        </div>
                                        <div className="panel-body">
                                            <div className="row">
                                                <div className="col-md-3">
                                                    <div className="form-group">
                                                        <label>Kategori Pengumuman *:</label>
                                                        <Field name="category"
                                                            className="form-control"
                                                            component="select"
                                                        >
                                                            <option>Pilih Kategori</option>
                                                            {categoryOption}
                                                        </Field>
                                                    </div>
                                                </div> 
                                                <div className="col-md-6">
                                                    <div className="form-group">
                                                        <label>Thumbnail Pengumuman :</label>
                                                        <input type="file" name="thumbnail" className="form-control" onChange={this.setThumbnail} />
                                                    </div>
                                                </div>  
                                                <div className="col-md-3">
                                                    <div className="form-group">
                                                        <label>Instansi Pengumuman *:</label>
                                                        <Field name="opd" className="form-control"
                                                            component="select">
                                                            <option>Pilih Instansi </option>
                                                            {opdOption}
                                                        </Field>
                                                    </div>
                                                </div>  
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <button type="button" className="btn btn-success" onClick={ handleSubmit(this.onClickSave.bind(this)) } >Publish</button>
                                                        <button type="button" className="btn btn-danger">Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                )}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    category : state.category,
    opd : state.opd
})

const mapDispatchToProps = dispatch => {
    return {
        getCategoryData : () => dispatch(fetchCategory()),
        getOpdListData : () => dispatch(getListOpd()),
        saveAnnouncementData : data => dispatch(saveAnnouncement(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: 'announcement'})(New))