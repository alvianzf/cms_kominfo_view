import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import Loading from '../../../component/loading'
import { fetchCategory } from '../../../redux/action/category'
import { getListOpd } from '../../../redux/action/opd'
import { EditorState, convertToRaw, ContentState, convertFromHTML} from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { reduxForm , Field } from 'redux-form'
import { getSingleAnnouncement, editAnnouncement } from '../../../redux/action/announcement'

class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            body : "",
            editorState: EditorState.createEmpty(),
            thumbnail : "",
            description : "",
            title : "",
            category : "",
            opd : ""
        }
    }

    componentDidMount = () => {
        this.props.getCategoryData()
        this.props.getOpdListData()
        this.props.getSingleAnnouncementData(this.props.match.params.id)
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
        this.setState({
            body : draftToHtml(convertToRaw(editorState.getCurrentContent()))
        })
    };

    setThumbnail = (e) => {
        this.setState({
            thumbnail : e.target.files[0]
        })
    }

    componentWillReceiveProps()
    {
        this.setState({
            title : this.props.announcement.single ? this.props.announcement.single.title : "",
            description : this.props.announcement.single ? this.props.announcement.single.description : "",
            category : this.props.announcement.single ? this.props.announcement.single.category : "",
            editorState: EditorState.createWithContent(
                ContentState.createFromBlockArray(
                    convertFromHTML(this.props.announcement.single ? this.props.announcement.single.body : "<p>No Content Yet</p>")
                )
            ),
        })
    }

    onChangeTitle = e => {
        this.setState({
            title : e.target.value
        })
    }

    onChangeDescription = (e) => {
        this.setState({
            description : e.target.value
        })
    }

    onChangeCategory = (e) => {
        this.setState({
            category : e.target.value
        })
    }

    setThumbnail = (e) => {
        this.setState({
            thumbnail : e.target.files[0]
        })
    }

    onClickSave = (e) => {
        console.log(this.state)
        const data = {
            title : this.state.title,
            description : this.state.description,
            body : this.state.body,
            thumbnail : this.state.thumbnail,
            thumbnail : this.state.category,
            slug : this.props.announcement.single ? this.props.announcement.single.slug : null,
            status : "Publish"
        }
        this.props.editAnnouncementData(data)
    } 

    render() {
        console.log(this.props.announcement)
        const { editorState } = this.state;
        const { handleSubmit } = this.props
        const categoryOption = this.props.category.data.map( category => (
            <option key={category._id} value={category._id} > {category.name} </option>
        ))
        const opdOption = this.props.opd.data.map(opd => (
            <option key={opd.id_opd} value={opd.link_opd} > {opd.nama_opd} </option>
        ))
        return (
            <div>
                { this.props.opd.loading ? (
                    <Loading />
                ): (
                    <div>
                    
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">Pengumuman</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li className="active">
                                        <strong>Pengumuman</strong>
                                    </li>
                                </ol>        
                            </div>
                        </div>
                        <form id="announcement">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <div className="panel-title">
                                                Edit Pengumuman
                                            </div>
                                        </div>
                                        <div className="panel-body">
                                            <div className="form-group">
                                                <label>Judul Pengumuman:</label>
                                                <input type="text" name="title" className="form-control" defaultValue={this.state.title} onChange={this.onChangeTitle.bind(this)}/>
                                            </div>
                                            <div className="form-group">
                                                <label>Descripsi Singkat Pengumuman:</label>
                                                <textarea name="description" className="form-control" rows={5} onChange={this.onChangeDescription.bind(this)} value={this.state.description}/>
                                            </div>
                                            <div className="form-group">
                                                <label>Tulis Pengumuman:</label>
                                                <Editor
                                                    editorState={editorState}
                                                    toolbarClassName="toolbarClassName"
                                                    wrapperClassName="demo-wrapper"
                                                    editorClassName="demo-editor"
                                                    onEditorStateChange={this.onEditorStateChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <div className="panel-title">
                                                Atribute Pengumuman
                                            </div>
                                        </div>
                                        <div className="panel-body">
                                            <div className="row">
                                                <div className="col-md-3">
                                                    <div className="form-group">
                                                        <label>Kategori Pengumuman *:</label>
                                                        <select name="category"
                                                            className="form-control"
                                                            component="select"
                                                            value={this.state.category}
                                                            onChange={this.onChangeCategory.bind(this)}
                                                        >
                                                            <option>Pilih Kategori</option>
                                                            {categoryOption}
                                                        </select>
                                                    </div>
                                                </div> 
                                                <div className="col-md-6">
                                                    <div className="form-group">
                                                        <label>Thumbnail Pengumuman :</label>
                                                        <input type="file" name="thumbnail" className="form-control" onChange={this.setThumbnail} />
                                                    </div>
                                                </div>  
                                                <div className="col-md-3">
                                                    <div className="form-group">
                                                        <label>Instansi Pengumuman *:</label>
                                                        <select name="opd" className="form-control"
                                                            component="select">
                                                            <option>Pilih Instansi </option>
                                                            {opdOption}
                                                        </select>
                                                    </div>
                                                </div>  
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <button type="button" className="btn btn-success" onClick={ this.onClickSave.bind(this) }>Publish</button>
                                                        <button type="button" className="btn btn-danger">Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                )}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    category : state.category,
    opd : state.opd,
    announcement : state.announcement
})

const mapDispatchToProps = dispatch => {
    return {
        getCategoryData : () => dispatch(fetchCategory()),
        getOpdListData : () => dispatch(getListOpd()),
        getSingleAnnouncementData : data => dispatch(getSingleAnnouncement(data)),
        editAnnouncementData : data => dispatch(editAnnouncement(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: 'announcement'})(Edit))