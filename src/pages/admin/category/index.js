import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ReactTable from 'react-table'
import "react-table/react-table.css"
import { reduxForm , Field } from 'redux-form'
import { connect } from 'react-redux'
import { fetchCategory,createCategory,deleteCategory,updateCategory } from '../../../redux/action/category'
import Loading from '../../../component/loading'

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            edit : false
        }
    }

    componentDidMount() {
        this.props.getCategoryData()
    }

    onPressSave = (value) => {
        const data = JSON.stringify(value)
        this.props.saveCategoryData(data)
        this.props.reset()
    }

    onPressUpdate = (value) => {
        this.props.updateCategoryData(value)
        this.setState({
            edit : false
        })
        this.props.change('name', null);
    }

    onPressDelete = (value) => {
        this.props.deleteCategoryData(value.original._id)
    }

    onPressEdit = (value) => {
        this.props.change('name', value.original.name);
        this.props.change('id', value.original._id);
        this.setState({
            edit : true
        })
    }

    render() {
        const { handleSubmit } = this.props
        return (
            <div>  
                { this.props.category.loading ? (
                    <Loading />
                ): (
                    <div>
                        <div className="page-title">
                            <div className="title-env">
                                <h1 className="title">Artikel Kategori</h1>
                            </div>
                            <div className="breadcrumb-env">
                                <ol className="breadcrumb bc-1">
                                    <li>
                                        <Link to="/"><i className="fa-home"></i>Home</Link>
                                    </li>
                                    <li className="active">
                                        <strong>Kategori</strong>
                                    </li>
                                </ol>
                                        
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-4">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Form kategori artikel
                                        </div>
                                    </div>
                                    <div className="panel-body">
                                        { this.state.edit ? (
                                            <form id="category" onSubmit={handleSubmit(this.onPressUpdate.bind(this))}>
                                                <div className="form-group">
                                                    <label> Nama Kategori : </label>
                                                    <Field name="id" type="text" id="id"
                                                        className="form-control hidden"
                                                        component="input"
                                                    />
                                                    <Field name="name" type="text" id="name"
                                                        className="form-control"
                                                        component="input"
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <button type="submit" className="btn btn-info btn-single pull-right">Update Kategori</button>
                                                </div>  
                                            </form>
                                        ) : (
                                            <form id="category" onSubmit={handleSubmit(this.onPressSave.bind(this))}>
                                                <div className="form-group">
                                                    <label> Nama Kategori : </label>
                                                    <Field name="name" type="text" id="name"
                                                        className="form-control"
                                                        component="input"
                                                        value={this.state.valueName}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <button type="submit" className="btn btn-info btn-single pull-right">Tambah Kategori</button>
                                                </div>  
                                            </form>
                                        )}
                                        
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-8">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="panel-title">
                                            Data kategori artikel
                                        </div>
                                    </div>
                                    <ReactTable
                                        data={this.props.category.data}
                                        columns={[
                                            {
                                                Header: "No",
                                                accessor: "id",
                                                maxWidth : 50,
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Nama Kategori",
                                                accessor : "name",
                                                style : {
                                                    "textAlign" : "center"
                                                }
                                            },
                                            {
                                                Header: "Aksi",
                                                accessor : "action",
                                                maxWidth : 100,
                                                sortable: false,
                                                filterable: false,
                                                style : {
                                                    "textAlign" : "center"
                                                },
                                                Cell: value => (
                                                    <div className="vertical-top">
                                                        <button style={{ marginBottom : 0 }} onClick={this.onPressEdit.bind(this, value)} className="btn btn-sm btn-icon btn-blue">
                                                            <i className="fa-pencil"></i>
                                                        </button>
                                                        <button style={{ marginBottom : 0 }} onClick={this.onPressDelete.bind(this, value)} className="btn btn-sm btn-icon btn-red">
                                                            <i className="fa-remove"></i>
                                                        </button>
                                                    </div>
                                                )
                                            }
                                        ]}
                                        defaultPageSize={10}
                                        className="table table-striped"
                                        filterable
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                )} 

            </div>
        )
    }
}

const mapStateToProps = state => ({
    category : state.category
})

const mapDispatchToProps = dispatch => {
    return {
        getCategoryData : () => dispatch(fetchCategory()),
        saveCategoryData : data => dispatch(createCategory(data)),
        deleteCategoryData : data => dispatch(deleteCategory(data)),
        updateCategoryData : data => dispatch(updateCategory(data))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: 'category'})(Index))