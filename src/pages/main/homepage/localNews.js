import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import history from '../../../history'

class LocalNews extends Component {
    onClickDetail = (id) => {
		history.push('/berita/opd/'+id)
	}
    render() {
        const renderList = this.props.articles.slice(0,12).map( article => (
            <li key={article._id}>
                <div className="xe-comment-entry">
                    <Link to={`/berita/opd/${article.slug}`} className="xe-user-img">
                        <img src={process.env.REACT_APP_BASE_URL + article.thumbnail} className="img-square" width="120"/>
                    </Link>
                    
                    <div className="xe-comment">
                        <Link to={`/berita/opd/${article.slug}`} className="xe-user-name">
                            <strong>{article.title}</strong>
                            <ul className="row child-title list-unstyled">
                                <li className="list-item col-md-4">Kategori : {article.category[0].name}</li>
                                <li className="list-item col-md-4">Tanggal Terbit : Mamak</li>
                                <li className="list-item col-md-4">Tanggal Tulis : Mamak</li>
                                <li className="list-item col-md-4">Penulis : {article.author.name}</li>
                                <li className="list-item col-md-4">Jumlah Viewer : {article.viewsCount}</li>
                                <li className="list-item col-md-4">Jumlah Komentar : {article.comments.length}</li>
                            </ul>
                        </Link>
                        <p>{article.description}</p>
                    </div>
                </div>
            </li>
        ))
        return (
            <div className="panel panel-default">
                <div className="panel-heading judul-panel">
                    BERITA DINAS KOMUNIKASI DAN INFORMATIKA
                </div>
                <div className="panel-body">

                    <div className="row">
                        <div className="col-md-12">
                            <div className="xe-widget xe-conversations" style={{ padding : 0}}>
                                <div className="xe-body">
                                    <ul className="list-unstyled">
                                        {renderList}
                                    </ul>
                                </div>
                                <div className="xe-footer">
                                <Link to="/berita/opd" className="btn btn-warning read-more"> Selengkapnya </Link>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        )
    }
}

export default LocalNews