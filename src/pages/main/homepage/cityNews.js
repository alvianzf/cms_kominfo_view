import React, { Component } from 'react'
import { Link } from 'react-router-dom';

class CityNews extends Component {
    render() {
        const renderList = this.props.news.slice(0,12).map( news => (
            <li key={news.id_berita}>
                <div className="xe-comment-entry">
                    <Link to={`/berita/umum/${news.link_berita}`} className="xe-user-img">
                        <img src={news.link_gambar_small} className="img-square" width="120"/>
                    </Link>
                    
                    <div className="xe-comment">
                        <Link to={`/berita/umum/${news.link_berita}`} className="xe-user-name">
                            <strong>{news.judul_berita}</strong>
                            <ul className="row child-title list-unstyled">
                                <li className="list-item col-md-4">Kategori : {news.kategori.kategori}</li>
                                <li className="list-item col-md-4">Tanggal Terbit : {`${news.tanggal_terbit} ${news.bulan_terbit} ${news.tahun_terbit}`}</li>
                                <li className="list-item col-md-4">Tanggal Tulis : {news.tanggal_post}</li>
                                <li className="list-item col-md-4">Penulis : {news.author.nama_penulis}</li>
                                <li className="list-item col-md-4">Jumlah Viewer : {news.viewsCount}</li>
                                <li className="list-item col-md-4">Jumlah Komentar :</li>
                                <li className="list-item col-md-4">OPD : {news.link_opd}</li>
                            </ul>
                        </Link>
                        <p>{news.isi_berita_ringkas}</p>
                    </div>
                </div>
            </li>
        ))
        return (
            <div className="panel panel-default">
                <div className="panel-heading judul-panel">
                    BERITA PEMKO
                </div>
                <div className="panel-body">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="xe-widget xe-conversations" style={{ padding : 0}}>
                                <div className="xe-body">
                                    <ul className="list-unstyled">
                                        {renderList}
                                    </ul>
                                </div>
                                <div className="xe-footer">
                                    <Link to="/berita/umum/" className="btn btn-warning read-more"> Selengkapnya </Link>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        )
    }
}

export default CityNews