import React, { Component } from 'react'
import CityNews from './cityNews'
import LocalNews from './localNews'
import ListGallery from './listGallery'
import ListEvent from '../../../component/listEvent'
import ListLatestNews from '../../../component/listLatestNews'
import { connect } from 'react-redux'
import { getNewsListData } from '../../../redux/action/news'
import { getListArticle } from '../../../redux/action/article'
import Loading from '../../../component/loading'

class Index extends Component {
    componentDidMount()
    {
        this.props.getNewsListDataFunc()
        this.props.getListArticleData()
    }
    render() {
        const news = this.props.news.data
        const articles = this.props.article.data

        return (
            <div>
                {this.props.news.loading ? (
                    <Loading />
                ): (
                    <div className="row">
                        <div className="col-md-4">
                            <ListEvent />
                            <ListLatestNews news={news}/>
                        </div>
                        <div className="col-md-8">
                            <div className="panel panel-default">
                                <div className="panel-body">
                                    <img src="https://via.placeholder.com/650x350" className="img-responsive" alt=""/>
                                </div>
                            </div>

                            <CityNews news={news} />
                            <LocalNews articles={articles} />

                            <div className="panel panel-default">
                                <div className="panel-heading judul-panel">
                                    Gallery 
                                </div>
                                <div className="panel-body">
                                    <ListGallery />
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
            
        )
    }
}

const mapStateToProps = state => ({
    news : state.news,
    article : state.article
})

const mapDispatchToProps = dispatch => {
    return {
        getNewsListDataFunc : () => dispatch(getNewsListData()),
        getListArticleData : () => dispatch(getListArticle())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)