import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class List extends Component {

    render() {
        const renderList = this.props.announcement.map( announcement => (
            <div key={ announcement._id} className="news-item">
                <div className="row">
                    <Link to={"/announcement/"+announcement.slug}>
                        <div className="col-md-3">
                            <img src={announcement.thumbnail ? process.env.REACT_APP_BASE_URL + announcement.thumbnail : 'https://via.placeholder.com/150'} alt="" className="img-responsive"/>
                        </div>
                    </Link>
                    <div className="col-md-9">
                        <Link to={"/announcement/"+announcement.slug}>
                            <h4 className="mg-t-0"> {announcement.title} </h4>
                        </Link>
                        <p>{announcement.description}</p>
                        <Link to={"/announcement/"+announcement.slug} className="btn btn-warning read-more"> View </Link>
                    </div>
                </div>
            </div>
        ))
        return (
            <div className="list-news">     
                {renderList}
            </div>
        )
    }
}

export default List