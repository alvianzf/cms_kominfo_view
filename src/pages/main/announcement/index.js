import React , { Component } from 'react'
import List from './list'
import ListEvent from '../../../component/listEvent'
import { connect } from 'react-redux'
import { getAllAnnouncement } from '../../../redux/action/announcement'
import Loading from '../../../component/loading'

class Index extends Component {
	componentDidMount()
	{
		this.props.getAllAnnouncementData()
	}
    render() {

		const announcement = this.props.announcement.data
        return (
			<div>
				{this.props.announcement.loading ? (
					<Loading />
				) : (
					<div className="row">
						<div className="col-md-8">
							<div className="panel panel-default">
								<div className="panel-heading judul-panel">
									KUMPULAN PENGUMUMAN DINAS
								</div>
								<div className="panel-body">
									<List announcement={announcement} />
								</div>
							</div>

                            <div className="panel panel-default">
								<div className="panel-heading judul-panel">
									KUMPULAN PENGUMUMAN PEMKO
								</div>
								<div className="panel-body">
									<List announcement={announcement} />
								</div>
							</div>
						</div>
						<div className="col-md-4">
							
							<ListEvent />

						</div>
					</div>
				)}
			</div>
            
        )
    }
}

const mapStateToProps = state => ({
	announcement : state.announcement
})

const mapDispatchToProps = dispatch => {
	return {
		getAllAnnouncementData : () => dispatch(getAllAnnouncement())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)