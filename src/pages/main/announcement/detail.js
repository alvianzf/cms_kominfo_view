import React , { Component } from 'react'
import ListEvent from '../../../component/listEvent'
import { connect } from 'react-redux'
import { getSingleAnnouncement } from '../../../redux/action/announcement'
import Loading from '../../../component/loading'

class Detail extends Component {
	componentDidMount()
	{
		this.props.getSingleAnnouncementData(this.props.match.params.id)
	}

    render() {
        if(this.props.announcement.single){
            var {_id, title, thumbnail, body } = this.props.announcement.single
        } else {
            var _id , title ,thumbnail ,body = null
        }
        return (
			<div>
				{this.props.announcement.loading ? (
				<Loading />
			) : (
				<div className="row">
					<div className="col-md-8">
                        <div key={_id} className="panel panel-default">
                            <div className="panel-heading judul-panel">
                                {title}
                            </div>
                            <div className="panel-body">
                                <img src={thumbnail ? process.env.REACT_APP_BASE_URL + thumbnail : 'https://via.placeholder.com/150'} className="img-responsive" alt=""/>
                                <p className="mg-t-15" dangerouslySetInnerHTML={{__html: body }} />
                            </div>
                        </div>
					</div>
					<div className="col-md-4">
						<ListEvent />

					</div>
				</div>
			)}
			</div>
        )
    }
}

const mapStateToProps = state => ({
	announcement : state.announcement,
})

const mapDispatchToProps = dispatch => {
	return {
		getSingleAnnouncementData : data => dispatch(getSingleAnnouncement(data))
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Detail)