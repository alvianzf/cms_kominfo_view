import React , { Component } from 'react'
import ListEvent from '../../../../component/listEvent'
import { connect } from 'react-redux'
import { fetchCategory } from '../../../../redux/action/category'
import { getNewsSingleData } from '../../../../redux/action/news'
import Loading from '../../../../component/loading'

class Detail extends Component {
	componentDidMount()
	{
		this.props.getListCategoryData()
		this.props.getSingleNews(this.props.match.params.id)
	}

    render() {
		const detail = this.props.news.detail ? this.props.news.detail.map( news => (
			<div key={news.id_berita} className="panel panel-default">
				<div className="panel-heading judul-panel">
					{news.judul_berita}
				</div>
				<div className="panel-body">
					<img src={news.link_gambar_big} className="img-responsive" alt=""/>
					<p className="mg-t-15" dangerouslySetInnerHTML={{__html: news.isi_berita }} />
				</div>
			</div>
		)) : null
        return (
			<div>
				{this.props.news.loading ? (
				<Loading />
			) : (
				<div className="row">
					<div className="col-md-8">
						{detail}
					</div>
					<div className="col-md-4">
						<ListEvent />

					</div>
				</div>
			)}
			</div>
        )
    }
}

const mapStateToProps = state => ({
	category : state.category,
	news : state.news
})

const mapDispatchToProps = dispatch => {
	return {
		getListCategoryData : () => dispatch(fetchCategory()),
		getSingleNews : data => dispatch(getNewsSingleData(data))
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Detail)