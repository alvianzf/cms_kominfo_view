import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import history from '../../../../history'

class ListNews extends Component {
    onClickDetail = (id) => {
		history.push('/berita/umum/'+id)
	}
    render() {
        const renderList = this.props.news.map( news => (
            <div key={ news.id_berita} className="news-item">
                <div className="row">
                    <Link to={"/berita/umum/"+news.link_berita}>
                        <div className="col-md-3">
                            <img src={news.link_gambar_small} alt="" className="img-responsive"/>
                        </div>
                    </Link>
                    <div className="col-md-9">
                        <Link to={"/berita/umum/"+news.link_berita}>
                            <h4 className="mg-t-0"> {news.judul_berita} </h4>
                            <ul className="row child-title list-unstyled">
                                <li className="list-item col-md-4">Kategori : {news.kategori.kategori}</li>
                                <li className="list-item col-md-4">Tanggal Terbit : {`${news.tanggal_terbit} ${news.bulan_terbit} ${news.tahun_terbit}`}</li>
                                <li className="list-item col-md-4">Tanggal Tulis : {news.tanggal_post}</li>
                                <li className="list-item col-md-4">Penulis : {news.author.nama_penulis}</li>
                                <li className="list-item col-md-4">Jumlah Viewer : {news.viewsCount}</li>
                                <li className="list-item col-md-4">Jumlah Komentar :</li>
                                <li className="list-item col-md-4">OPD : {news.link_opd}</li>
                            </ul>
                        </Link>
                        <p>{news.isi_berita_ringkas}</p>
                        <Link to={"/berita/umum/"+news.link_berita} className="btn btn-warning read-more"> View </Link>
                    </div>
                </div>
            </div>
        ))
        return (
            <div className="list-news">     
                {renderList}
            </div>
        )
    }
}

export default ListNews