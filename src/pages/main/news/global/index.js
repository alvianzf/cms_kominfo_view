import React , { Component } from 'react'
import ListNews from './listNews'
import ListEvent from '../../../../component/listEvent'
import { connect } from 'react-redux'
import { getNewsAllData } from '../../../../redux/action/news'
import Loading from '../../../../component/loading'

class Index extends Component {
	componentDidMount()
	{
		this.props.getNewsAllDataFunc()
	}
    render() {

		const news = this.props.news.data
        return (
			<div>
				{this.props.news.loading ? (
					<Loading />
				) : (
					<div className="row">
						<div className="col-md-8">
							<div className="panel panel-default">
								<div className="panel-heading judul-panel">
									KUMPULAN BERITA PEMKO
								</div>
								<div className="panel-body">
									<ListNews news={news} />
									{/* <ul className="pagination">
										<li><a href="#0"><i className="fa-angle-left"></i></a></li>
										<li><a href="#0">1</a></li>
										<li className="active"><a href="#0">2</a></li>
										<li><a href="#0">3</a></li>
										<li className="disabled"><a href="#0">4</a></li>
										<li><a href="#0">5</a></li>
										<li><a href="#0">6</a></li>
										<li><a href="#0"><i className="fa-angle-right"></i></a></li>
									</ul> */}
								</div>
							</div>
						</div>
						<div className="col-md-4">
							
							<ListEvent />

						</div>
					</div>
				)}
			</div>
            
        )
    }
}

const mapStateToProps = state => ({
	news : state.news
})

const mapDispatchToProps = dispatch => {
	return {
		getNewsAllDataFunc : () => dispatch(getNewsAllData())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)