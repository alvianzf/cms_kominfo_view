import React, { Component } from 'react'

class ListCategory extends Component {
    render() {
        const renderList = this.props.categories.map( category => (
            <li key={category._id}> {category.name} </li>
        ))
        return (
            <div className="list-news">
                <ul className="item-list">
                    {renderList}
                </ul>
            </div>
        )
    }
}

export default ListCategory