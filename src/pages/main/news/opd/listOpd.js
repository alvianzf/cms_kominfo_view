import React, { Component } from 'react'

class ListOpd extends Component {
    render() {
        const renderList = this.props.opds.map( opd => (
            <li key={opd.link_opd}> {opd.nama_opd} </li>
        ))
        return (
            <div className="list-news">
                <ul className="item-list">
                    {renderList}
                </ul>
            </div>
        )
    }
}

export default ListOpd