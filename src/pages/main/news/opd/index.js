import React , { Component } from 'react'
import ListCategory from './listCategory'
import ListNews from './listNews'
import ListOpd from './listOpd'
import ListEvent from '../../../../component/listEvent'
import { connect } from 'react-redux'
import { getListArticle } from '../../../../redux/action/article'
import { fetchCategory } from '../../../../redux/action/category'
import { getListOpd } from '../../../../redux/action/opd'
import Loading from '../../../../component/loading'

class Index extends Component {
	componentDidMount()
	{
		this.props.getListArticleData()
		this.props.getListCategoryData()
		this.props.getListOpdData()
	}

    render() {
		const opds = this.props.opd.data
		const categories = this.props.category.data
		const articles = this.props.article.data
        return (
			<div>
				{this.props.article.loading ? (
					<Loading />
				) : (
					<div className="row">
						<div className="col-md-4">
							<ListEvent />
							<div className="panel panel-default">
								<div className="panel-heading judul-panel">
									KATEGORI BERITA
								</div>
								<div className="panel-body">
									<ListCategory categories={categories}/>
								</div>
							</div>
							<div className="panel panel-default">
								<div className="panel-heading judul-panel">
									DAFTAR OPD
								</div>
								<div className="panel-body">
									<ListOpd opds={opds} />
								</div>
							</div>
						</div>
						<div className="col-md-8">
							<div className="panel panel-default">
								<div className="panel-heading judul-panel">
									KUMPULAN BERITA DINAS
								</div>
								<div className="panel-body">
									<ListNews articles={articles} />
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
            
        )
    }
}

const mapStateToProps = state => ({
	article : state.article,
	category : state.category,
	opd : state.opd
})

const mapDispatchToProps = dispatch => {
	return {
		getListArticleData : () => dispatch(getListArticle()),
		getListCategoryData : () => dispatch(fetchCategory()),
		getListOpdData : () => dispatch(getListOpd())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)