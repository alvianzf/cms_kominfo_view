import React , { Component } from 'react'
import ListEvent from '../../../../component/listEvent'
import ListCategory from './listCategory'
import { connect } from 'react-redux'
import { getSingleArticle } from '../../../../redux/action/article'
import Loading from '../../../../component/loading'
import { reduxForm , Field } from 'redux-form'
import { getListCommentData,createComment } from '../../../../redux/action/comment'
import { fetchCategory } from '../../../../redux/action/category'

class Detail extends Component {
	componentDidMount()
	{
		this.props.getSingleArticleData(this.props.match.params.id)
		this.props.getCommentData(this.props.match.params.id)
		this.props.getListCategoryData()
	}

	saveComment = values => {
		const data = {
			...values,
			slug : this.props.match.params.id
		}
		this.props.saveCommentData(data)
		this.props.reset()
	}
	
    render() {
		const detail = this.props.article.detail
		const { handleSubmit } = this.props
		const listComment = this.props.comment.data.map( comment => (
			<p key={comment.id}>
				<small>{comment.body}</small>
			</p>
		))
		const categories = this.props.category.data
        return (
			<div>
				{this.props.article.loading ? (
					<Loading />
				) : (
					<div className="row">
						<div className="col-md-4">
							<ListEvent />
							<div className="panel panel-default">
								<div className="panel-heading judul-panel">
									KATEGORI BERITA
								</div>
								<div className="panel-body">
									<ListCategory categories={categories}/>
								</div>
							</div>
						</div>
						<div className="col-md-8">
							<div className="panel panel-default">
								<div className="panel-heading judul-panel">
									{detail ? detail.title : null }
									<ul className="row child-title list-unstyled">
										<li className="list-item col-md-4">Kategori : {detail ? detail.category[0].name : null}</li>
										<li className="list-item col-md-4">Tanggal Terbit : Mamak</li>
										<li className="list-item col-md-4">Tanggal Tulis : Mamak</li>
										<li className="list-item col-md-4">Penulis : {detail ? detail.author.name : null}</li>
										<li className="list-item col-md-4">Jumlah Viewer : {detail ? detail.viewsCount : null}</li>
										<li className="list-item col-md-4">Jumlah Komentar : {detail ? detail.comments.length : null}</li>
									</ul>
								</div>
								<div className="panel-body">
									{ detail ? (
										<img src={process.env.REACT_APP_BASE_URL + detail.thumbnail} className="img-responsive" alt=""/>
									) : (
										<img src="https://via.placeholder.com/650x350" className="img-responsive" alt=""/>
									)}
									<p className="mg-t-15" dangerouslySetInnerHTML={{__html: detail ? detail.body : null }} />
								</div>
							</div>
							<div className="panel panel-default">
								<div className="panel-heading">
									Data Singkat Penulis
								</div>
								<div className="panel-body">
									<div className="row">
										<div className="col-md-4">
											<img src="https://ui-avatars.com/api/?name=Suryandi" className="img-square" width="120"/>
										</div>
										<div className="col-md-8">
											<p><b>Nama Penulis : </b> {detail ? detail.author.name : null}</p>
											<p><b>OPD Penulis : </b> Dinas Komunikasi dan Informatika </p>
										</div>
									</div>
								</div>	
							</div>
							<div className="panel panel-default">
								<div className="panel-heading">
									Tulis Komentar
								</div>
								<div className="panel-body">
									<form id="comment">
										<div className="form-group">
											<Field name="body" type="text" id="body"
												className="form-control"
												component="textarea" rows={5}
												placeholder = "Tulis Komentar Anda Disini"
											/>
										</div>
										<div className="form-group">
											<button type="button" className="btn btn-success" onClick={ handleSubmit(this.saveComment.bind(this)) } >Submit</button>
										</div>
									</form>
								</div>	
							</div>
							<blockquote className="blockquote blockquote-default">
								<p>
									<strong>Komentar</strong>
								</p>
								{listComment}
							</blockquote>
						</div>
					</div>
				)}
			</div>
            
        )
    }
}

const mapStateToProps = state => ({
	category : state.category,
	opd : state.opd,
	article : state.article,
	comment : state.comment
})

const mapDispatchToProps = dispatch => {
	return {
		getSingleArticleData : (data) => dispatch(getSingleArticle(data)),
		getCommentData : (data) => dispatch(getListCommentData(data)),
		saveCommentData : data => dispatch(createComment(data)),
		getListCategoryData : () => dispatch(fetchCategory()),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: 'comment'})(Detail))