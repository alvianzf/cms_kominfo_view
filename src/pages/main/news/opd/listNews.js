import React, { Component } from 'react'
import history from '../../../../history'

class ListNews extends Component {
    onClickDetail = (id) => {
		history.push('/berita/opd/'+id)
	}
    render() {
        const renderList = this.props.articles.map( article => (
            <div key={ article._id} className="news-item">
                <div className="row">
                    <div className="col-md-3">
                        <a href="#3" onClick={this.onClickDetail.bind(this, article.slug)}>
                            <img src={process.env.REACT_APP_BASE_URL + article.thumbnail} alt="" className="img-responsive"/>
                        </a>
                    </div>
                    <div className="col-md-9">
                        <a href="#3" onClick={this.onClickDetail.bind(this, article.slug)}>
                            <h4 className="mg-t-0"> {article.title} </h4>
                            <ul className="row child-title list-unstyled">
                                <li className="list-item col-md-4">Kategori : {article.category[0].name}</li>
                                <li className="list-item col-md-4">Tanggal Terbit : Mamak</li>
                                <li className="list-item col-md-4">Tanggal Tulis : Mamak</li>
                                <li className="list-item col-md-4">Penulis : {article.author.name}</li>
                                <li className="list-item col-md-4">Jumlah Viewer : {article.viewsCount}</li>
                                <li className="list-item col-md-4">Jumlah Komentar : {article.comments.length}</li>
                            </ul>
                        </a>
                        <p>{article.description}</p>
                        <a href="#3" onClick={this.onClickDetail.bind(this, article.slug)} className="btn btn-warning read-more"> View </a>
                    </div>
                </div>
            </div>
        ))
        return (
            <div className="list-news">
                {renderList}
            </div>
        )
    }
}

export default ListNews