import React, { Component } from 'react'
import ListGallery from './listGallery'
import ListMonth from './listMonth'
import ListEvent from '../../../component/listEvent'

class Index extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-8">
                    <div className="panel panel-default">
						<div className="panel-heading judul-panel">
							KUMPULAN GALLERY
						</div>
						<div className="panel-body">
                            <ListGallery />

                            <ul className="pagination">
								<li><a href="#0"><i className="fa-angle-left"></i></a></li>
								<li><a href="#0">1</a></li>
								<li className="active"><a href="#0">2</a></li>
								<li><a href="#0">3</a></li>
								<li className="disabled"><a href="#0">4</a></li>
								<li><a href="#0">5</a></li>
								<li><a href="#0">6</a></li>
								<li><a href="#0"><i className="fa-angle-right"></i></a></li>
							</ul>
						</div>
					</div>
                </div>
                <div className="col-md-4">
					<ListEvent />
                    <div className="panel panel-default">
						<div className="panel-heading judul-panel">
							BULAN GALLERY
						</div>
						<div className="panel-body">
                            <ListMonth />
						</div>
					</div>
                </div>
            </div>
        )
    }
}

export default Index