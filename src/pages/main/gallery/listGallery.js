import React, { Component } from 'react'

class ListGallery extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="xe-widget xe-single-news">
                        <div className="xe-image">
                            <img src="https://via.placeholder.com/350" className="img-responsive" alt=""/>
                            <span className="xe-gradient"></span>
                        </div>
                        
                        <div className="xe-details">                            
                            <h3>
                                <a href="#0">Gallery</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ListGallery