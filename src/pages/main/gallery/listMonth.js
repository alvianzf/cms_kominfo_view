import React, { Component } from 'react'

class ListMonth extends Component {
    render() {
        return (
            <div className="list-news">
                <ul className="item-list">
                    <li> January 2019 </li>
                    <li> February 2019 </li>
                    <li> March 2019 </li>
                    <li> April 2019 </li>
                </ul>
            </div>
        )
    }
}

export default ListMonth