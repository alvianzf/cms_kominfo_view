import React, { Component } from 'react'
import { reduxForm , Field } from 'redux-form'
import { connect } from 'react-redux'
import { loginRequest } from '../../redux/action/login' 
import $ from 'jquery'

class Index extends Component {
    constructor(props)
    {
        super(props)
        document.body.classList.add('login-page');
        setTimeout(
            function(){ 
                $(".fade-in-effect").addClass('in'); 
            }, 
            1
        );
    }

    submit = (values) => {
        this.props.onPressLogin(values)
    }

    render() {
        const { handleSubmit } = this.props
        return (
            <div className="row">
                <div className="col-sm-6">
                    <div className="errors-container">
                                    
                    </div>
                    <form method="post" id="login" className="login-form fade-in-effect" onSubmit={handleSubmit(this.submit)}>
                    
                        <div className="login-header">
                            <a href="dashboard-1.html" className="logo">
                                <h3>Portal Kedinasan Pemerintah Kota Tanjungpinang</h3>
                            </a>
                            
                            <p>Silakan Masuk</p>
                        </div>
        
                        
                        <div className="form-group">
                            <label className="control-label">Nomor Induk Pegawai</label>
                            <Field name="idpeg" type="text" id="idpeg"
                                className="form-control input-dark"
                                component="input"
                            />
                        </div>
                        
                        <div className="form-group">
                            <label className="control-label">Password</label>
                            <Field name="password" type="password" id="passwd"
                                className="form-control input-dark"
                                component="input"
                            />
                        </div>
                        
                        <div className="form-group">
                            <button type="submit" className="btn btn-dark  btn-block text-left">
                                <i className="fa-lock"></i>
                                Log In
                            </button>
                        </div>                            
                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    login: state.login,
})

const mapDispatchToProps = dispatch => {
    return {
        onPressLogin : data => dispatch(loginRequest(data))
    }
}

export default reduxForm({ form: 'login'})(connect(mapStateToProps, mapDispatchToProps )(Index))