import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { Provider } from 'react-redux'
import { Router } from 'react-router'
import configureStore from './store'
import history from './history'

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}><App /></Router> 
    </Provider>,
    document.getElementById('root')
);

