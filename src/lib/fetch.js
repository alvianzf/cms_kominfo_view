
const API_URL = process.env.REACT_APP_API_URL

export const postNoAuth = (body, API) => {
    return fetch(API_URL + API, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: body,
    })
}

export const getNoAuth = (API) => {
    return fetch(API_URL + API, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
}

export const get = async (API) => {
    var token = localStorage.getItem('token')
    return fetch(API_URL + API, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : `Token ${token}`
        }
    })
}

export const post = async (body, API) => {
    var token = localStorage.getItem('token')
    return fetch(API_URL + API, {
        method : 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : `Token ${token}`
        },
        body : body
    })
}

export const put = async (body, API) => {
    var token = localStorage.getItem('token')
    return fetch(API_URL + API, {
        method : 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : `Token ${token}`
        },
        body : body
    })
}

export const postImg = async (body, API) => {
    var token = await localStorage.getItem('token');
    return fetch(API_URL + API, {
        method : 'POST',
        headers: {
            'Authorization' : `Token ${token}`
        },
        body : body
    })
}

export const putImg = async (body, API) => {
    var token = await localStorage.getItem('token');
    return fetch(API_URL + API, {
        method : 'PUT',
        headers: {
            'Authorization' : `Token ${token}`
        },
        body : body
    })
}
